<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!--  會員資料取回頁面 可選擇電話或是郵件 -->
<html>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">    
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    
             
   <head>

  </head>
 <%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/logrtv.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
<body style="background-color:#eaedea;">

<div class="container marketing" style="margin-top:5rem;">



 <form action="../index_getback_pwd" method="post">
   <div class="row">  
      <div class="col-lg-4"></div>
      
     <div class="col-lg-4">
     <h5 class="form-text text-muted"> 請輸入員工帳號與員工信箱  </h5>
        <div class="form-group">
    <label for="exampleInputEmail1">員工帳號 </label>
    <input type="text" class="form-control" id="account" name="account" required="required">
    
        </div>
      <div class="form-group">
    <label >員工信箱</label>
    <input type="email" class="form-control" id="mail" name="mail" required="required">
       </div>   
        <button type="submit" class="btn btn-primary">取回密碼</button>     
       </div>      
     <div class="col-lg-4"> </div> 
    </div>
    </form>
 
  
   

       

 </div>
 
  
   </body>
</html>