<%@page import="dsg.Report_By_Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/boss/index.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/boss/index.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
<%
Report_By_Order rbo_today = (Report_By_Order)session.getAttribute("rbo_today");
Report_By_Order rbo_yesterday = (Report_By_Order)session.getAttribute("rbo_yesterday");
%>
<br>
<br>
<br>
<br>
<br>
<br>
			<div class="row">

			<div class="col-lg-6">
			<div class="py-3 text-center">
					<h2>當日訂單狀況</h2>
				</div>
				<div class="card border-info mb-3">
					<div class="card-body text-dark">
					<div class="row">
<%if(rbo_today!=null) {%>
					<div class="col-lg-6">
					<div>外送訂單筆數：<%=rbo_today.getDev_count() %></div>
					<div>外送訂單金額：<%=rbo_today.getDev_sum() %></div>
					<div>門市訂單筆數：<%=rbo_today.getStore_count() %></div>
					<div>門市訂單金額：<%=rbo_today.getStore_sum() %></div>
					<div>合計訂單總額：<%=rbo_today.getTotal_sum() %></div>
				
					</div>
					<div class="col-lg-6">
					<div>定單取消筆數：<%=rbo_today.getBad_count() %></div>
					<div>定單取消金額：<%=rbo_today.getBad_sum() %></div>
					<div>銷售第一名:<%=rbo_today.getFst_name() %>/<%=rbo_today.getFst_qty() %>(杯)</div>
					<div>銷售第二名:<%=rbo_today.getSec_name() %>/<%=rbo_today.getSec_qty() %>(杯)</div>
					<div>銷售第三名:<%=rbo_today.getTrd_name() %>/<%=rbo_today.getTrd_qty() %>(杯)</div>
					</div>
<%}else{ %>
					<div class="col-lg-6">
					<div>外送訂單筆數：0</div>
					<div>外送訂單金額：0</div>
					<div>門市訂單筆數：0</div>
					<div>門市訂單金額：0</div>
					<div>合計訂單總額：0</div>
				
					</div>
					<div class="col-lg-6">
					<div>定單取消筆數：0</div>
					<div>定單取消金額：0</div>
					<div>銷售第一名:NULL / 0 (杯)</div>
					<div>銷售第二名:NULL / 0 (杯)</div>
					<div>銷售第三名:NULL / 0 (杯)</div>
					</div>
<%} %>
					</div>
					
					
					
						
						
                        
					</div>
					
				</div>
			</div>
			
						<div class="col-lg-6">
						<div class="py-3 text-center">
					<h2>前日訂單狀況</h2>
				</div>
				<div class="card border-info mb-3">
					<div class="card-body text-dark">
					<div class="row">
					<div class="col-lg-6">
<%if(rbo_yesterday!=null) {%>
					<div>外送訂單筆數：<%=rbo_yesterday.getDev_count() %></div>
					<div>外送訂單金額：<%=rbo_yesterday.getDev_sum() %></div>
					<div>門市訂單筆數：<%=rbo_yesterday.getStore_count() %></div>
					<div>門市訂單金額：<%=rbo_yesterday.getStore_sum() %></div>
					<div>合計訂單總額：<%=rbo_yesterday.getTotal_sum() %></div>
				
					</div>
					<div class="col-lg-6">
					<div>定單取消筆數：<%=rbo_yesterday.getBad_count() %></div>
					<div>定單取消金額：<%=rbo_yesterday.getBad_sum() %></div>
					<div>銷售第一名:<%=rbo_yesterday.getFst_name() %>/<%=rbo_yesterday.getFst_qty() %>(杯)</div>
					<div>銷售第二名:<%=rbo_yesterday.getSec_name() %>/<%=rbo_yesterday.getSec_qty() %>(杯)</div>
					<div>銷售第三名:<%=rbo_yesterday.getTrd_name() %>/<%=rbo_yesterday.getTrd_qty() %>(杯)</div>
					</div>
<%}else{ %>
					<div class="col-lg-6">
					<div>外送訂單筆數：0</div>
					<div>外送訂單金額：0</div>
					<div>門市訂單筆數：0</div>
					<div>門市訂單金額：0</div>
					<div>合計訂單總額：0</div>
				
					</div>
					<div class="col-lg-6">
					<div>定單取消筆數：0</div>
					<div>定單取消金額：0</div>
					<div>銷售第一名:NULL / 0 (杯)</div>
					<div>銷售第二名:NULL / 0 (杯)</div>
					<div>銷售第三名:NULL / 0 (杯)</div>
					</div>
<%} %>
					</div>
					
					
					
						
						
                        
					</div>
					
				</div>
			</div>
			
		</div>


   </body>
</html>