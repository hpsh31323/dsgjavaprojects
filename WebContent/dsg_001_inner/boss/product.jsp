<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>
  <%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/boss/product.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/boss/product.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>

<%
String aditem = null;
String rvitem = null;
String catset = null;
String pg = (String)session.getAttribute("pg");
if(pg==null) {
	aditem = "show active";
}else{
	switch(Integer.parseInt(pg)){
		case 1:
			aditem = "active";
			break;
		case 2:
			rvitem = "active";
			break;
		case 3:
			catset = "active";
			break;
	}
}

%>
<body style="background-color:#eff9ff;">

  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action <%=aditem %> text-center" id="list-aditem-list" data-toggle="list" href="#list-aditem" role="tab" aria-controls="aditem">新增商品</a>
      <a class="list-group-item list-group-item-action <%=rvitem %> text-center"        id="list-rvitem-list" data-toggle="list" href="#list-rvitem" role="tab" aria-controls="rvitem">修改商品</a>
      <a class="list-group-item list-group-item-action <%=catset %> text-center"        id="list-catset-list" data-toggle="list" href="#list-catset" role="tab" aria-controls="catset">類別調整</a>

    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade <%="show "+aditem %>" id="list-aditem" role="tabpanel" aria-labelledby="list-aditem-list"><%@ include file="product/aditem.jsp"%></div>
      <div class="tab-pane fade <%="show "+rvitem %>" id="list-rvitem" role="tabpanel" aria-labelledby="list-rvitem-list"><%@ include file="product/rvitem.jsp"%></div>
      <div class="tab-pane fade <%="show "+catset %>" id="list-catset" role="tabpanel" aria-labelledby="list-catset-list"><%@ include file="product/catset.jsp"%></div>
   
    </div>
  </div>
</div>
</div>
   </body>
</html>