<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
<%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/boss/crew.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/boss/crew.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>

<%
String addnew = null;
String revise = null;
String admin = null;
String pg = (String)session.getAttribute("pg");
if(pg==null) {
	addnew = "active";
}else{
	switch(Integer.parseInt(pg)){
		case 1:
			addnew = "active";
			break;
		case 2:
			revise = "active";
			break;
		case 3:
			admin = "active";
			break;
	}
}

%>

<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action <%=addnew %> text-center" id="list-addnew-list" data-toggle="list" href="#list-addnew" role="tab" aria-controls="addnew">人員新增</a>
      <a class="list-group-item list-group-item-action <%=revise %> text-center"        id="list-revise-list" data-toggle="list" href="#list-revise" role="tab" aria-controls="revise">修改資料</a>
      <a class="list-group-item list-group-item-action <%=admin %> text-center"        id="list-admin-list" data-toggle="list" href="#list-admin" role="tab" aria-controls="admin">管理員密碼</a>
     
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade <%="show "+addnew %>" id="list-addnew" role="tabpanel" aria-labelledby="list-addnew-list"> <%@ include file="crew/addnew.jsp"%></div>
      <div class="tab-pane fade <%="show "+revise %>" id="list-revise" role="tabpanel" aria-labelledby="list-revise-list"><%@ include file="crew/revise.jsp"%></div>
      <div class="tab-pane fade <%="show "+admin %>" id="list-admin" role="tabpanel" aria-labelledby="list-admin-list"><%@ include file="crew/admin.jsp"%></div>
   
    </div>
  </div>
</div>
</div>
   </body>
</html>