<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active text-center" id="list-aboutme-list" data-toggle="list" href="#list-aboutme" role="tab" aria-controls="aboutme">店家資料</a>
      <a class="list-group-item list-group-item-action text-center"        id="list-deliverrule-list" data-toggle="list" href="#list-deliverrule" role="tab" aria-controls="deliverrule">外送規則</a>
      <a class="list-group-item list-group-item-action text-center"        id="list-clientnote-list" data-toggle="list" href="#list-clientnote" role="tab" aria-controls="clientnote">客戶註記列表</a>
     
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-aboutme" role="tabpanel" aria-labelledby="list-aboutme-list"> <%@ include file="service/aboutme.jsp"%></div>
      <div class="tab-pane fade" id="list-deliverrule" role="tabpanel" aria-labelledby="list-deliverrule-list"><%@ include file="service/deliverrule.jsp"%></div>
      <div class="tab-pane fade" id="list-clientnote" role="tabpanel" aria-labelledby="list-clientnote-list"><%@ include file="service/clientnote.jsp"%></div>
   
    </div>
  </div>
</div>
</div>
   </body>
</html>