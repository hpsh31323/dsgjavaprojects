<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- 店長頁面的 head -->

<html>
<title>Insert title here</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
            <link href="../../css/bootstrap.min.css" rel="stylesheet">   
            <script src="https://code.jquery.com/jquery.js"></script>
            <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
            <script src="../../js/bootstrap.min.js"></script>
       
<body>
<nav class="navbar navbar-light  navbar-expand-md  fixed-top" style="background-color: #6fcaf7;">
<!-- 圖片的位置和連結,目前是連到首頁 -->
    <a class="navbar-brand" href="../../boss_index"><img src="../../img/dsg_logo.png" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
 

        <li class="nav-item">              
          <a class="nav-link" href="../../report">銷售報表</a>
        </li>
        
        

        <li class="nav-item">
          <a class="nav-link" href="../../product">產品設定</a>
        </li>

    <!--  
        <li class="nav-item">
          <a class="nav-link" href="promot.jsp">行銷活動</a>          
        </li>
        
        
         <li class="nav-item">
          <a class="nav-link" href="service.jsp">服務設定</a>          
        </li>
        -->
     
        <li class="nav-item">
          <a class="nav-link" href="../../crew">人員登錄</a>
        </li>
        
     
      

 
 <!-- 二個按紐在頁面最右上,登出和購物車  -->       
      </ul>   
        &nbsp;			
        <button class="btn btn-outline-info " type="button"
				type="button" data-toggle="modal" data-target="#logout">
				<img src="../../img/logout.png" height="25">
			</button>
    </div>     
 </nav>
 
 <div class="modal fade" id="crewmodal" tabindex="-1" role="dialog" aria-hidden="false">
  <div class="modal-dialog  " role="document">
    <div class="modal-content">
    </div>
  </div>
</div>
  
				<!-- Modal -->
				<div class="modal fade" id="logout" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">登出</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">


								<div>確認登出</div>


							</div>
							<div class="modal-footer">
							<form action="../../inner_logout" method="post">
							<input type="hidden" name="path" value="dsg_001_inner/index.jsp">
								<button type="submit" class="btn btn-danger">登出</button>
									</form>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">取消</button>
							</div>
						</div>
					</div>
				</div>
  
</body>
</html>