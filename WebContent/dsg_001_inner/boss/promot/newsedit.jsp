<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<title>訊息設定</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">


<head>
<body style="background-color: #eaedea;">


	<table class="table table-bordered text-center ">

		<thead>
			<tr>
				<th scope="col" width="20%">圖片</th>
				<th scope="col" width=""><a class="text-muted">img <br>請依1:1比例編修圖檔，上傳限制為500K
				</a></th>

			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">檔案</th>
				<td><form>
						<div class="form-group  ">
							<input type="file" class="form-control-file" id="uploadpic">
						</div>
					</form></td>


			</tr>
			<tr>
				<th scope="row">主題</th>
				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="">
					</div></td>

			</tr>
			<tr>
				<th scope="row">標題</th>
				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="">
					</div></td>

			</tr>
			<tr>
				<th scope="row">簡述</th>
				<td><div>
						<textarea class="form-control" id="newstext" rows="3"> 請儘量於五十字內簡單描述</textarea>

					</div></td>

			</tr>

			<tr>
				<th scope="row">顯示時間</th>
				<td>
					<form>
						<div class="form-row align-items-center">
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2"
									id="inlineFormCustomSelect">
									<option selected>月</option>
									<option value="1">一月</option>
									<option value="2">二月</option>
									<option value="3">三月</option>
								</select>
							</div>
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2"
									id="inlineFormCustomSelect">
									<option selected>日</option>
									<option value="1">１</option>
									<option value="2">２</option>
									<option value="3">３</option>
								</select>
							</div>
						</div>
					</form>
				</td>

			</tr>
			<tr>
				<th scope="row">結束時間</th>
				<td>
					<form>
						<div class="form-row align-items-center">
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2"
									id="inlineFormCustomSelect">
									<option selected>月</option>
									<option value="1">一月</option>
									<option value="2">二月</option>
									<option value="3">三月</option>
								</select>
							</div>
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2"
									id="inlineFormCustomSelect">
									<option selected>日</option>
									<option value="1">１</option>
									<option value="2">２</option>
									<option value="3">３</option>
								</select>
							</div>
						</div>
					</form>
				</td>

			</tr>
			<tr>
				<th scope="row"></th>
				<td><div>
						<button type="button" class="btn btn-primary">確定送出</button>
					</div></td>

			</tr>






		</tbody>
	</table>
	<table class="table table-bordered text-center">
		<thead>
			<tr>
				<th scope="col">圖片</th>
				<th scope="col" width="10%">主題</th>
				<th scope="col" width="15%">標題</th>
				<th scope="col" width="30%">內容</th>
				<th scope="col" width="14%">檔期</th>
				<th scope="col" width="9%">修改</th>
				<th scope="col" width="9%">刪除</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row"><img src="../../img/q.jpg" width="100%">
				</th>
				<td>新品推薦</td>
				<td>大小珍珠粉圓貢丸茶</td>
				<td>所有口感一次滿足，話就當年白居易就是喝了這個，才想出“大珠小珠落玉盤”的千古絕句，為琵琶行畫龍點睛，傳頌至今。</td>
				<td><div>2019/01/14</div>
					<div>2019/01/14</div></td>

				<td>
					<button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button>
				</td>
				<td>
					<button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button>
				</td>
			</tr>

			<!-- if 修改 -->
			<tr>
				<th scope="row"><img src="../../img/q.jpg" width="100%">
				</th>
				<td><input type="text" class="form-control" id="filename"
					placeholder=" 點修改跳出"></td>
				<td><input type="text" class="form-control" id="filename"
					placeholder=" 點修改跳出"></td>
				<td><textarea class="form-control" id="newstext" rows="3"> 點修改跳出</textarea></td>

				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="2019/01/14">
					</div>
					<div>
						<input type="text" class="form-control" id="filename"
							placeholder="2019/01/14 ">
					</div></td>
				<td>
					<button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button>
				</td>
				<td>
					<button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button>
				</td>
			</tr>

			<!-- if 結束 -->
			<tr>
				<th scope="row"><img src="../../img/n.jpg" width="100%">
				</th>
				<td>店長特調</td>
				<td>小丸子愛撒嬌系列，內湖店限定</td>
				<td>內湖店長本身營養學系出身，針對愛美女性推出保養茶飲，喝了就像小丸子，永遠年輕長不大，只在內湖店快來嘗鮮吧～</td>
				<td><div>2019/02/14</div>
					<div>2019/03/14</div></td>

				<td><button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button></td>
				<td><button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button></td>
			</tr>
			<tr>
				<th scope="row"><img src="../../img/p.jpg" width="100%">
				</th>
				<td>台灣之光</td>
				<td>美國門市接獲川普訂單</td>
				<td>白宮消息指出，為慶祝歐巴馬卸任三週年，川普宴廣邀媒體餐敘，指名本店要黑黑黑奶茶，獻給歐巴馬和他妻子女兒，剛好三個黑！</td>
				<td><div>2019/02/01</div>
					<div>2019/02/14</div></td>
				<td><button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button></td>
				<td><button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button></td>
			</tr>
		</tbody>
	</table>


</body>


</html>


