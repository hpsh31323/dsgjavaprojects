<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<title>輪播設定</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">


<head>
<body style="background-color: #eaedea;">




	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="20%">圖片</th>
				<th scope="col" width=""><a class="text-muted">img <br>請依16:9比例編修圖檔，上傳限制為500K
				</a></th>

			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">檔案</th>
				<td><form>
						<div class="form-group  ">
							<input type="file" class="form-control-file" id="uploadpic">
						</div>
					</form></td>


			</tr>
			<tr>
				<th scope="row">檔案名稱</th>
				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="">
					</div></td>

			</tr>

			<tr>
				<th scope="row">開始時間</th>
				<td>
					<form>
						<div class="form-row align-items-center">
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2" id="slidesetsm">
									<option selected>月</option>
									<option value="1">一月</option>
									<option value="2">二月</option>
									<option value="3">三月</option>
								</select>
							</div>
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2" id="slidesetsd">
									<option selected>日</option>
									<option value="1">１</option>
									<option value="2">２</option>
									<option value="3">３</option>
								</select>
							</div>
						</div>
					</form>
				</td>

			</tr>
			<tr>
				<th scope="row">結束時間</th>
				<td>
					<form>
						<div class="form-row align-items-center">
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2" id="slidesetem">
									<option selected>月</option>
									<option value="1">一月</option>
									<option value="2">二月</option>
									<option value="3">三月</option>
								</select>
							</div>
							<div class="col-auto my-1">
								<select class="custom-select mr-sm-2" id="slideseted">
									<option selected>日</option>
									<option value="1">１</option>
									<option value="2">２</option>
									<option value="3">３</option>
								</select>
							</div>
						</div>
					</form>
				</td>

			</tr>
			<tr>
				<th scope="row">連結網頁</th>
				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="">
					</div></td>

			</tr>

			<tr>
				<th scope="row"></th>
				<td><div>
						<button type="button" class="btn btn-primary">確定送出</button>
					</div></td>

			</tr>


		</tbody>
	</table>



	<table class="table table-bordered text-center">
		<thead>
			<tr>
				<th scope="col">圖片</th>
				<th scope="col" width="20%">檔名</th>
				<th scope="col" width="15%">檔期</th>
				<th scope="col">連結</th>
				<th scope="col" width="10%">修改</th>
				<th scope="col" width="10%">刪除</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row"><img src="../../img/3.jpg" width="100%">
				</th>
				<td>測試１</td>
				<td><div>2019/01/14</div>
					<div>2019/01/14</div></td>
				<td>./deliver.jsp</td>
				<td>
					<button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button>
				</td>
				<td>
					<button type="submit" class="btn btn-outline-danger btn-sm">
						<small>刪除</small>
					</button>
				</td>
			</tr>

			<!-- if 修改 -->
			<tr>
				<th scope="row"><img src="../../img/3.jpg" width="100%">
				</th>
				<td><input type="text" class="form-control" id="filename"
					placeholder=" 點修改跳出"></td>
				<td><div>
						<input type="text" class="form-control" id="filename"
							placeholder="2019/01/14">
					</div>
					<div>
						<input type="text" class="form-control" id="filename"
							placeholder="2019/01/14 ">
					</div></td>
				<td><input type="text" class="form-control" id="filename"
					placeholder=" ./deliver.jsp"></td>
				<td>
					<button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button>
				</td>
				<td>
					<button type="submit" class="btn btn-outline-danger btn-sm">
						<small>刪除</small>
					</button>
				</td>
			</tr>

			<!-- if 結束 -->
			<tr>
				<th scope="row"><img src="../../img/2.jpg" width="100%">
				</th>
				<td>情人節活動</td>
				<td><div>2019/02/14</div>
					<div>2019/03/14</div></td>
				<td>./member.jsp</td>
				<td><button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button></td>
				<td><button type="submit" class="btn btn-outline-danger btn-sm">
						<small>刪除</small>
					</button></td>
			</tr>





			<tr>
				<th scope="row"><img src="../../img/1.jpg" width="100%">
				</th>
				<td>春節活動</td>
				<td><div>2019/02/01</div>
					<div>2019/02/14</div></td>
				<td>./logrtv.jsp</td>
				<td><button type="submit" class="btn btn-info btn-sm">
						<small>修改</small>
					</button></td>
				<td><button type="submit" class="btn btn-outline-danger btn-sm">
						<small>刪除</small>
					</button></td>
			</tr>

		</tbody>

	</table>


</body>

</html>