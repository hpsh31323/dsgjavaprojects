<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
    <title>行銷活動</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active text-center" id="list-slide-list" data-toggle="list" href="#list-slide" role="tab" aria-controls="slide">首頁輪播</a>
      <a class="list-group-item list-group-item-action text-center"        id="list-news-list" data-toggle="list" href="#list-news" role="tab" aria-controls="news">訊息發佈</a>
      <a class="list-group-item list-group-item-action text-center"        id="list-promotion-list" data-toggle="list" href="#list-promotion" role="tab" aria-controls="promotion">商品促銷</a>
     
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-slide" role="tabpanel" aria-labelledby="list-general-list"> <%@ include file="promot/slideset.jsp"%></div>
      <div class="tab-pane fade" id="list-news" role="tabpanel" aria-labelledby="list-news-list"><%@ include file="promot/newsedit.jsp"%></div>
      <div class="tab-pane fade" id="list-promotion" role="tabpanel" aria-labelledby="list-promotion-list"><%@ include file="promot/promotion.jsp"%></div>
   
    </div>
  </div>
</div>
</div>
   </body>
</html>