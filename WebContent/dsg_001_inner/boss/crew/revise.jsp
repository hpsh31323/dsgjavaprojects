<%@page import="dsg.Employee"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<title>修改資料</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">
<%
Employee [] employees = (Employee[])session.getAttribute("employees");
int x = employees.length;
int index = 0;
%>

<head>
<body style="background-color: #eaedea;">

	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="8%"><small>員工編號</small></th>
				<th scope="col" width="12%"><small>姓名</small></th>
				<th scope="col" width="14%"><small>手機</small></th>
				<th scope="col" width="10%"><small>郵件</small></th>
				<th scope="col" width="10%"><small>建檔日期</small></th>
				<th scope="col" width="10%"><small>修改</small></th>

			</tr>
		</thead>
		<tbody>
<%for(int i = 0;i<x;i++){ %>
			<tr>
				<td class="align-middle"><small><%=employees[i].getAccount() %></small></td>
				<td class="align-middle"><small><%=employees[i].getName() %></small></td>
				<td class="align-middle"><small><%=employees[i].getTel() %></small></td>
				<td class="align-middle"><small><%=employees[i].getMail() %></small></td>
				<td class="align-middle"><small><%=employees[i].getOnboard() %></small></td>
				<td class="align-middle">
					<button type="button" class="btn btn-info btn-sm"
						data-toggle="modal" data-target="#rvcrew<%=i %>">
						<small>修改</small>
					</button>
				</td>

			</tr>
<%} %>
		</tbody>
	</table>

	<!-- 對話框帶入修改內容。。。。這個對話框只有連第一個修改 ，要核對id  值 -->
<%for(int i = 0;i<x;i++){ %>
	<div class="modal fade" id="rvcrew<%=i %>" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog " role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="rvcrew<%=i %>">修改內容</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				
						<form method="post" action="../../crew_revise">

								<!-- 修改內容開始 -->
									<input type="hidden"
									class="form-control" id="e_id" name="e_id" placeholder="" 
									value="<%=employees[i].getId() %>" required>
								<label for="name">中文姓名</label> 
									<input type="text"
									class="form-control" id="name" name="name" placeholder="" 
									value="<%=employees[i].getName() %>" required>
								<label for="phone">行動電話</label> 
									<input type="text"
									class="form-control" id="phone" name="phone" placeholder="" 
									value="<%=employees[i].getTel() %>" required> 
								<label for="mail">電子郵件</label> 
									<input
									type="email" class="form-control" id="mail" name="mail" placeholder=""
									value="<%=employees[i].getMail() %>" required> 
								<label for="onboard">在職＆離職</label> 
									<input
									type="radio" class="form-control" id="incumbent1" name="incumbent" 
									value="1" checked> 在職
									<input
									type="radio" class="form-control" id="incumbent2" name="incumbent" 
									value="2"> 離職
						
							<div class="modal-footer">
								<br>
								<div>
									<button type="submit" class="btn btn-primary">確定送出</button>
								</div>
								<div>
									<button type="button" class="btn btn-danger" data-dismiss="modal"
						aria-label="Close">取消</button>
								</div>
							</div>
						</form>

						<!-- 修改內容送出結束 -->
					</div>
				</div>
			</div>
		</div>
<%} %>

</body>

</html>