<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>
<%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/boss/report.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/boss/report.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
<%
String general = null;
String byitem = null;
String pg = (String)session.getAttribute("pg");
if(pg==null) {
	general = "show active";
}else{
	switch(Integer.parseInt(pg)){
		case 1:
			general = "active";
			break;
		case 2:
			byitem = "active";
			break;
	}
}

%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action <%=general %> text-center" id="list-general-list" data-toggle="list" href="#list-general" role="tab" aria-controls="general">業績列表</a>
      <a class="list-group-item list-group-item-action <%=byitem %> text-center"        id="list-byitem-list" data-toggle="list" href="#list-byitem" role="tab" aria-controls="byitem">單品統計</a>
   <!--  
      <a class="list-group-item list-group-item-action text-center"        id="list-promotion-list" data-toggle="list" href="#list-promotion" role="tab" aria-controls="promotion">促銷統計</a>
      <a class="list-group-item list-group-item-action text-center" id="list-period-list" data-toggle="list" href="#list-period" role="tab" aria-controls="period">時段比較</a>
      -->
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade <%="show "+general %>" id="list-general" role="tabpanel" aria-labelledby="list-general-list"><%@ include file="report/general.jsp"%></div>
      <div class="tab-pane fade <%="show "+byitem %>" id="list-byitem" role="tabpanel" aria-labelledby="list-byitem-list"><%@ include file="report/singleitem.jsp"%></div>
      <!--  
      <div class="tab-pane fade" id="list-promotion" role="tabpanel" aria-labelledby="list-promotion-list">,./,./,./,./,./,./,./,./.,/,./,./,./</div>
      <div class="tab-pane fade" id="list-period" role="tabpanel" aria-labelledby="list-period-list">452345234tert245tg4eryt245yt</div>
   -->
    </div>
  </div>
</div>
</div>
   </body>
</html>