<%@page import="dsg.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<title>商品促銷</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">


<head>
<body style="background-color: #eaedea;">
	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="">品名</th>
				<th scope="col" width="24%">單價(中杯/大杯)</th>
				<th scope="col" width="">冷熱</th>
				<th scope="col" width="">銷售</th>
				<th scope="col" width="12%">類別</th>
				<th scope="col" width="10%">排序</th>
				<th scope="col" width="8%">速改</th>
				<th scope="col" width="8%">修改</th>
		<!--  		<th scope="col" width="8%">刪除</th> -->
			</tr>
		</thead>
		<!-- first product -->
<% 
Product [] products = (Product[])session.getAttribute("products");
String [] p_class2 = (String[])session.getAttribute("p_class");
for(Product p : products){
%>
		
		<tbody>
		
			<tr>
			<form method="post" action="../../product_rvitem">
				<!-- 品名　 -->
				<td class="align-middle"><%=p.getName() %>
				<input type="hidden" name="pd_id" value="<%=p.getId() %>">
				</td>
				<!-- 單價　 -->
				<td class="align-middle">
					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
              <select class="custom-select mr-sm-2" id="pri1" name="price_m">
              <%if(p.getPrice_m()==0){ %>
       			 <option value="0" selected="selected">0</option>
       			 <%}else{ %>
         		<option value="0">0</option>
       			 <%} %>
        	<%for(int i =20;i<=95;i+=5){
        	if(p.getPrice_m()==i){
        	%>
        	<option value="<%=i %>" selected="selected"><%=i %></option>
        	<% }else{ %> 
        	<option value="<%=i %>" ><%=i %></option>
   			 <%     }
  			  }%>
      		</select> 
						</div>
						<div class="col form-group form-control-sm col-form-label-sm">
              <select class="custom-select mr-sm-2" id="pri2" name="price_l">
              <%if(p.getPrice_l()==0){ %>
        <option value="0" selected="selected">0</option>
        <%}else{ %>
         <option value="0">0</option>
        <%} %>
        <%for(int i =20;i<=95;i+=5){
        	if(p.getPrice_l()==i){
        	%>
        <option value="<%=i %>" selected="selected"><%=i %></option>
        <% }else{ %> 
        <option value="<%=i %>" ><%=i %></option>
    <%     }
    }%>
      </select> 
						</div>
					</div>
				</td>
				<!-- 冷熱　 -->
				<td>
					<div class="form-check">
					<% 
					String cold = (p.getCold()==1)?"checked='checked'":"";
					String hot = (p.getHot()==1)?"checked='checked'":"";
					%>
						<input class="form-check-input" type="checkbox" value="1"
							id="defaultCheck1" name="pd_cold" <%=cold %>> <label class="form-check-label"
							for="defaultCheck1"> 冷 </label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="1"
							id="defaultCheck2" name="pd_hot" <%=hot %>> <label class="form-check-label"
							for="defaultCheck2"> 熱 </label>
					</div>
				</td>
				<!-- 銷售　 -->

				<td>
<%if(p.getAvaliable()==1){ %>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="pd_avaliable"
							id="exampleRadios1" value="1" checked="checked"> <label
							class="form-check-label" for="exampleRadios1"> 開 </label>
					</div>

					<div class="form-check disabled">
						<input class="form-check-input" type="radio" name="pd_avaliable"
							id="exampleRadios3" value="0"> <label
							class="form-check-label" for="exampleRadios3"> 關 </label>
					</div>
					<%}else{ %>
						<div class="form-check">
						<input class="form-check-input" type="radio" name="pd_avaliable"
							id="exampleRadios1" value="1" > <label
							class="form-check-label" for="exampleRadios1"> 開 </label>
					</div>

					<div class="form-check disabled">
						<input class="form-check-input" type="radio" name="pd_avaliable"
							id="exampleRadios3" value="0" checked="checked"> <label
							class="form-check-label" for="exampleRadios3"> 關 </label>
					</div>
					<%} %>
				</td>
				<!-- 類別　 -->
				<td>
					<div class="form-row">
						<select
							class="col custom-select  form-control-sm col-form-label-sm"
							id="slidesetsd" name="pd_class">
        <%
        for(int i=0 ; i < p_class2.length ; i++){
        if(p.getClass_id()==(i+1)){
        %>
        <option value="<%=i+1 %>" selected="selected"><%=p_class2[i] %></option>
        <%}else{ %>
        <option value="<%=i+1 %>" ><%=p_class2[i] %></option>
        <% 
        } 
        }
        %>
      </select>
					</div>

				</td>

				<!-- 排序　 -->
				<td>

					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
						<select
							class="col custom-select  form-control-sm col-form-label-sm"
							id="priority" name="pd_priority">
        <%
        for(int i=0 ; i < 10 ; i++){
        if(p.getPriority()==(i+1)){
        %>
        <option value="<%=i+1 %>" selected="selected"><%=i+1 %></option>
        <%}else{ %>
        <option value="<%=i+1 %>" ><%=i+1 %></option>
        <% 
        } 
        }
        %>
      </select>
						</div>
					</div>
				</td>




				<!-- 速改　 -->
				<td class="align-middle">
					<button type="submit" class="btn btn-info btn-sm">
						<small>速改</small>
					</button>
				</td>
</form>


				<!-- 修改　 -->
				<td class="align-middle">
					<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#rvitem<%=p.getId() %>">
					
						<small>修改</small>
					</button>
				</td>

				<!-- 刪除　 -->
				<!--  
				<td class="align-middle">
				<form action="../../product_rvitem_delete" method="post">
				<input type="hidden" name="pd_id" value="">
					<button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button>
					-->
					</form>
				</td>
			</tr>
			</tbody>
			
			<%
}
%>
	</table>

<!-- 如果有修改，用對話框帶入aditm內容。。。。這個對話框只有連第一個修改 -->

<% 
for(Product p : products){
%>
<div class="modal fade" id="rvitem<%=p.getId() %>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="rvitem<%=p.getId() %>">修改內容</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <!-- aditem 的內容 -->
 <form method="post" enctype="multipart/form-data" action="../../product_rvitem_dialog">
<table class="table table-bordered text-center ">

  <thead>
  <!--  
    <tr>
      <th scope="col" width="20%">圖片</th>
      <th scope="col" width=""><a class="text-muted">jpg / png / jpeg <br>請依1:1比例編修圖檔，上傳限制為500K</a></th>     
    </tr>
    -->
  </thead>
  <tbody>
   <!--  
    <tr>
      <th scope="row" >檔案</th><td>

      <div class="form-group  ">
      <input type="file" class="form-control-file" id="uploadpic" name="pd_pic">
      </div></td>    
    </tr>
    -->
      <tr>
      <th scope="row">品名</th>
      <td><div >
    <input type="text" class="form-control" id="filename" placeholder="" name="1" value="<%=p.getName() %>"> 
    </div></td>  
    </tr>
          <tr>
      <th scope="row">類別</th>
      <td><div >
         <div class="col-auto  ">
       <select class="custom-select mr-sm-2" id="saleendd" name="2">
        <% 
        for(int i=0 ; i < p_class2.length ; i++){
        	if(p.getClass_id()==(i+1)){
        %>
        <option value="<%=i+1 %>" selected="selected"><%=p_class2[i] %></option>
        <%  }else{ %>
        <option value="<%=i+1 %>"><%=p_class2[i] %></option>
        <% 
        }
        } %>
      </select>
      </div> 
    </div></td>  
    </tr>
    
      <tr>
      <th scope="row">簡述 </th>
      <td><div >
      <textarea class="form-control" id="newstext" rows="3" name="3" value="<%=p.getNote() %>" placeholder="請儘量於五十字內簡單描述" ></textarea>
      </div></td>
      </tr>
 
       <tr>
      <th scope="row">單價</th>
      <td>
      <div class="form-row">

       
        <div class=" col form-group "><label>500CC(中)</label>
              <select class="custom-select mr-sm-2" id="pri1" name="4">
              <%if(p.getPrice_m()==0){ %>
        <option value="0" selected="selected">0</option>
        <%}else{ %>
         <option value="0">0</option>
        <%} %>
        <%for(int i =20;i<=95;i+=5){
        	if(p.getPrice_m()==i){
        	%>
        <option value="<%=i %>" selected="selected"><%=i %></option>
        <% }else{ %> 
        <option value="<%=i %>" ><%=i %></option>
    <%     }
    }%>
      </select> 
       </div>
     
        &emsp;

        <div class=" col form-group">
        <label>750CC(大)</label>
              <select class="custom-select mr-sm-2" id="pri2" name="5">
              <%if(p.getPrice_l()==0){ %>
        <option value="0" selected="selected">0</option>
        <%}else{ %>
         <option value="0">0</option>
        <%} %>
        <%for(int i =20;i<=95;i+=5){
        	if(p.getPrice_l()==i){
        	%>
        <option value="<%=i %>" selected="selected"><%=i %></option>
        <% }else{ %> 
        <option value="<%=i %>" ><%=i %></option>
    <%     }
    }%>
      </select> 
       </div>

      </div>
      </td>
      </tr>
 
      <tr>
      <th scope="row">冷熱飲設定</th>
      <td>
      <div class="form-row">
      <%
      if(p.getCold()==1){
      %>
          <div class=" col form-group">
       <input class="form-check-input" type="checkbox" id="ckl" name="6" value="1" checked="checked"><label>冷飲</label></div>
<% }else{ %>
          <div class=" col form-group">
       <input class="form-check-input" type="checkbox" id="ckl" name="6" value="1"><label>冷飲</label></div>
<%} %>
      &emsp;
            <%
      if(p.getHot()==1){
      %>
        <div class=" col form-group ">
       <input class="form-check-input" type="checkbox" id="ckg" name="7" value="1" checked="checked"><label>熱飲</label></div>
       </div>
     <%}else{ %>
             <div class=" col form-group ">
       <input class="form-check-input" type="checkbox" id="ckg" name="7" value="1"><label>熱飲</label></div>
       </div>
     <%} %>
      </td>
      </tr>
     <tr>
      <th scope="row"></th>
      <td>
      <div >
      <input type="hidden" name="8" value="<%=p.getId() %>">
   <button type="submit" class="btn btn-primary">確定送出</button>
							</div>
  </td>
     
    </tr>
 
  
 
 


</tbody>
</table>
</form>

      <!-- aditem 的內容結束 -->
  
      </div>
     
    </div>
  </div>
</div>
<%
}
%>




</body>




</body>
</html>