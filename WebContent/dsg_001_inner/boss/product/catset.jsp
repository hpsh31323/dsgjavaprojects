<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<title>商品促銷</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">


<head>
<body style="background-color: #eaedea;">


<!--  
<form action="../../product_catset" method="post">
	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="25%">類別</th>
				<th scope="col" width="">說明</th>
			  	<th scope="col" width="9%">排序</th> 

				<th scope="col" width="16%">新增</th>
			</tr>
		</thead>
		-->
		<!-- add new category -->
	<!--	<tbody>
			<tr> -->

				<!-- 類別　 -->
	<!--			<td class="align-middle">
					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
							<input type="text" class="form-control " id="addcat"
								placeholder=" " name="class_name" required="required">
						</div>
					</div>
				</td>
				-->
				<!-- 說明　 -->
	<!--			<td class="align-middle">
					<div class="form-group">

						<textarea class="form-control" id="catset" rows="2" placeholder="添加一些描述，前台暫時不會看到" name="class_note"></textarea>
					</div>
				</td>

				 排序　
				<td class="align-middle">

					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
							<input type="text" class="form-control " id="filename"
								placeholder=" ">
						</div>
					</div>
				</td>

-->

				<!-- confirm　 -->
		<!--		<td class="align-middle">
					<button type="submit" class="btn btn-danger btn-sm">
						<small>新增</small>
					</button>
				</td>
			</tr>


		</tbody>
	</table>
</form>
-->
	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="25%">類別</th>
				<th scope="col" width="">說明</th>
			<!--  <th scope="col" width="9%">排序</th> -->	
				<th scope="col" width="16%">修改</th>
			<!-- <th scope="col" width="8%">刪除</th> -->
			</tr>
		</thead>
		<!-- first product -->
		<tbody>



			<!-- first category -->
<% 
String [] p_class3 = (String [])session.getAttribute("p_class");
String [] p_c_note = (String [])session.getAttribute("p_c_note");
for(int i = 0 ; i < p_class3.length;i++){
%>
<form action="../../product_catset_revise" method="post">
			<tr>
				<!-- 類別　 -->
				<td class="align-middle">
					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
							<input type="text"  class="form-control"  id="addcat" name="class_name"
								 placeholder="" value="<%=p_class3[i] %>" required="required">
						</div>
					</div>
				</td>
				<!-- 說明　 -->
				<td class="align-middle">
					<div class="form-group">

						<textarea class="form-control" id="catset" rows="2" name="note" ><%=p_c_note[i] %></textarea>
					</div>
				</td>

				<!-- 排序　 
				<td class="align-middle">

					<div class="form-row">
						<div class="col form-group form-control-sm col-form-label-sm">
							<input type="text" class="form-control " id="filename"
								placeholder="1">
						</div>
					</div>
				</td>
-->

				<!-- 修改　 -->
				<td class="align-middle">
				<input type="hidden" name="class_id" value="<%=i+1 %>">
					<button type="submit" class="btn btn-info btn-sm">

						<small>修改</small>
					</button>
				</td>
</form>
<%} %>
				<!-- 刪除　 
				<td class="align-middle">
					<button type="submit" class="btn btn-danger btn-sm">
						<small>刪除</small>
					</button>
				</td>
			</tr>
			-->
		</tbody>
	</table>





</body>




</body>
</html>