<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="BIG5">
<title>新增商品</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
                
   <head>   
<body style="background-color:#eaedea;">
 <form method="post" enctype="multipart/form-data" action="../../product_aditem">
<table class="table table-bordered text-center ">

  <thead>
    <tr>
      <th scope="col" width="20%">圖片</th>
      <th scope="col" width=""><a class="text-muted">jpg / png / jpeg <br>請依1:1比例編修圖檔，上傳限制為500K</a></th>     
    </tr>
  </thead>
  <tbody>
  
    <tr>
      <th scope="row" >檔案</th><td>

      <div class="form-group  ">
      <input type="file" class="form-control-file" id="uploadpic" name="pd_pic">
      </div></td>

    
    </tr>
      <tr>
      <th scope="row">品名</th>
      <td><div >
    <input type="text" class="form-control" id="filename" placeholder="" name="1"> 
    </div></td>  
    </tr>
          <tr>
      <th scope="row">類別</th>
      <td><div >
         <div class="col-auto  ">
       <select class="custom-select mr-sm-2" id="saleendd" name="2">
        <option selected>類</option>
        <%
        String [] p_class = (String[])session.getAttribute("p_class");
        for(int i=0 ; i < p_class.length ; i++){
        %>
        <option value="<%=i+1 %>"><%=p_class[i] %></option>
        <%} %>
      </select>
      </div> 
    </div></td>  
    </tr>
    
      <tr>
      <th scope="row">簡述 </th>
      <td><div >
      <textarea class="form-control" id="newstext" rows="3" name="3" placeholder="請儘量於五十字內簡單描述"></textarea>
      </div></td>
      </tr>
 
       <tr>
      <th scope="row">單價</th>
      <td>
      <div class="form-row">
      <!-- 
        <div class=" col form-group">
         
       <input class="form-check-input" type="checkbox" id="ckm" name="4" value="1"> <label>500CC(中)</label></div>
        -->
       
        <div class=" col form-group "><label>500CC(中)</label>
              <select class="custom-select mr-sm-2" id="pri1" name="4">
        <option value="0" selected>0</option>
        <option value="20">20</option>
        <option value="25">25</option>
        <option value="30">30</option>
        <option value="35">35</option>
        <option value="40">40</option>
        <option value="45">45</option>
        <option value="50">50</option>
        <option value="55">55</option>
        <option value="60">60</option>
        <option value="65">65</option>
        <option value="70">70</option>
        <option value="75">75</option>
        <option value="80">80</option>
        <option value="85">85</option>
        <option value="90">90</option>
        <option value="95">95</option>
      </select> 
       </div>
     
        &emsp;
      <!--  
          <div class=" col form-group">
       <input class="form-check-input" type="checkbox" id="ckl" name="6" value="1"><label>750CC(大)</label>
       </div>
       -->
        <div class=" col form-group">
        <label>750CC(大)</label>
                    <select class="custom-select mr-sm-2" id="pri2" name="5">
        <option value="0" selected>0</option>
        <option value="20">20</option>
        <option value="25">25</option>
        <option value="30">30</option>
        <option value="35">35</option>
        <option value="40">40</option>
        <option value="45">45</option>
        <option value="50">50</option>
        <option value="55">55</option>
        <option value="60">60</option>
        <option value="65">65</option>
        <option value="70">70</option>
        <option value="75">75</option>
        <option value="80">80</option>
        <option value="85">85</option>
        <option value="90">90</option>
        <option value="95">95</option>
      </select> 
       </div>
       <!--  
      &emsp;

        <div class=" col form-group ">
       <input class="form-check-input" type="checkbox" id="ckg"><label>1000CC(巨)</label>
       </div>
       
        <div class=" col form-group">
       <input type="text" class="form-control" id="pri3" placeholder="">
       </div>
       -->
      </div>
      </td>
      </tr>
 
      <tr>
      <th scope="row">冷熱飲設定</th>
      <td>
      <div class="form-row">
      
          <div class=" col form-group">
       <input class="form-check-input" type="checkbox" id="ckl" name="6" value="1" checked="checked"><label>冷飲</label></div>

      &emsp;
        <div class=" col form-group ">
       <input class="form-check-input" type="checkbox" id="ckg" name="7" value="1" checked="checked"><label>熱飲</label></div>
       
  

       </div>
     
      </td>
      </tr>
 
 <!--  
      <tr>
      <th scope="row">季節開放 </th>
      <td >

      
  <div class="form-row ">
    <div class="col-auto  ">    
      <select class="custom-select mr-sm-2" id="salestartm">
        <option selected>月</option>
        <option value="1">一月</option>
        <option value="2">二月</option>
        <option value="3">三月</option>
      </select>
      </div>
       <div class="col-auto  ">
       <select class="custom-select mr-sm-2" id="salestartd">
        <option selected>日</option>
        <option value="1">１</option>
        <option value="2">２</option>
        <option value="3">３</option>
      </select>
      </div> 
      <div> -至- </div>
      <div class="form-row align-items-center">
    <div class="col-auto  ">    
      <select class="custom-select mr-sm-2" id="saleendm">
        <option selected>月</option>
        <option value="1">一月</option>
        <option value="2">二月</option>
        <option value="3">三月</option>
      </select>
      </div>
       <div class="col-auto  ">
       <select class="custom-select mr-sm-2" id="saleendd">
        <option selected>日</option>
        <option value="1">１</option>
        <option value="2">２</option>
        <option value="3">３</option>
      </select>
      </div> 
  </div> 
       
  </div>    
      </td>
   
    </tr>
    
      <tr>
      <th scope="row">排序</th>
      <td><div >
    <input type="text" class="form-control" id="filename" placeholder=""> 
    </div></td>  
    </tr>
    -->
     <tr>
      <th scope="row"></th>
      <td><div >
   <button type="submit" class="btn btn-primary">確定送出</button>
  </div></td>
     
    </tr>
 
  
 
 


</tbody>
</table>
</form>
   
 
   </body>
   

</html>


