<%@page import="dsg.Report_By_Order"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="BIG5">
<title>Insert title here</title>
</head>
<%
	Calendar calendar = Calendar.getInstance();
	int year1 = (session.getAttribute("year1")!=null)?Integer.parseInt((String)session.getAttribute("year1")):calendar.get(Calendar.YEAR);
	int month1 = (session.getAttribute("month1")!=null)?Integer.parseInt((String)session.getAttribute("month1")):calendar.get(Calendar.MONTH);
	int day1 = (session.getAttribute("day1")!=null)?Integer.parseInt((String)session.getAttribute("day1")):calendar.get(Calendar.DAY_OF_MONTH);
	int year2 = (session.getAttribute("year2")!=null)?Integer.parseInt((String)session.getAttribute("year2")):calendar.get(Calendar.YEAR);
	int month2 = (session.getAttribute("month2")!=null)?Integer.parseInt((String)session.getAttribute("month2")):calendar.get(Calendar.MONTH);
	int day2 = (session.getAttribute("day2")!=null)?Integer.parseInt((String)session.getAttribute("day2")):calendar.get(Calendar.DAY_OF_MONTH);
	Report_By_Order rbo = (Report_By_Order)session.getAttribute("report_By_Order");
%>
<script type="text/javascript">
	function onload(y) //y為年份
	{
		var year = parseInt(y);//取得年份   
		var yearList = document.getElementById("yearListFrom");
		for (var i = 0; i < (year - (year - 15)); i++) {
			var addYear = year - i;
			yearList.options[i] = new Option(addYear + "年", addYear);
		}
		yearChangeFrom();

	}

	function yearChangeFrom() {
		var monthList = document.getElementById("monthListFrom");
		monthList.innerHTML = "";
		for (var i = 0; i < 12; i++) {
			monthList.options[i] = new Option(i + 1 + "月", i + 1);
		}
		var dayList = document.getElementById("dayListFrom");
		dayList.innerHTML = "";
		for (var i = 0; i < 31; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}

	function monthChangeFrom(m) {//m為月份  
		var year = parseInt(document.getElementById("yearListFrom").value);
		var day = 31;
		switch (parseInt(m))//判斷月份並對應月份天數  
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = 28;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = 30;
			break;
		}
		if (day == 28)//二月份的情况下  
		{
			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))//判斷是否為閏年  
			{
				day = 29;
			}
		}
		var dayList = document.getElementById("dayListFrom");
		dayList.innerHTML = "";
		for (var i = 0; i < day; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}

	function yearChangeTo(y) {
		var monthList = document.getElementById("monthListTo");
		monthList.innerHTML = "";
		for (var i = 0; i < 12; i++) {
			monthList.options[i] = new Option(i + 1 + "月", i + 1);
		}
		var dayList = document.getElementById("dayListTo");
		dayList.innerHTML = "";
		for (var i = 0; i < 31; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
		onload(y);
	}

	function monthChangeTo(m) {//m為月份  
		var year = parseInt(document.getElementById("yearListTo").value);
		var day = 0;
		switch (parseInt(m))//判斷月份並對應月份天數  
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = 28;
			break;
		case 4:
		case 6:
		case 9:

	case 11:
			day = 30;
			break;
		}
		if (day == 28)//二月份的情况下  
		{
			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))//判斷是否為閏年  
			{
				day = 29;
			}
		}
		var dayList = document.getElementById("dayListTo");
		dayList.innerHTML = "";
		for (var i = 0; i < day; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}
</script>

<body style="background-color: #ecedea;">
	<div class="container marketing">
		<div class="py-4 text-center">
			<h5>
				<a class="text-muted"> 請選取時間範圍</a>
			</h5>
			<form action="../../report_general" method="post">
			<table class="table text-center ">
				<thead>
					<tr>
						<th scope="col">
							<div class="form-row">
								<label>起：</label> <select
									class="col custom-select  form-control-sm" id="yearListFrom" name="yearfrom"
									onchange="yearChangeFrom()">
									<%
										for (int i = 15; i > 0; i--) { // 抓取15年
									%>
									<option value="<%=year2 - i%>"><%=year2 - i%>年
									</option>
									<%
										}
									%>
									<option selected value="<%=year1 %>"><%=year1 %>年
									</option>
								</select> 
								<select class="col custom-select  form-control-sm" name="monthfrom"
									id="monthListFrom" onchange="monthChangeFrom(this.value)">
									<%
										for (int i = 0 ; i <= month2; i++) {
											if (i != month1) {
									%>
									<option value="<%=i%>"><%=i+1%>月
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i+1%>月
									</option>
									<%
										}
										}
									%>
								</select> <select class="col custom-select form-control-sm"
									id="dayListFrom" name="dayfrom">
									<%
										for (int i = 1; i <= day2; i++) {
											if (i != day1) {
									%>
									<option value="<%=i%>"><%=i%>日
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i%>日
									</option>
									<%
										}
										}
									%>
								</select>
							</div>
						</th>
						<th scope="col"></th>
						<th scope="col">

							<div class="form-row">
								<label>迄：</label> <select
									class="col custom-select  form-control-sm" name="yearto"
									id="yearListTo" onchange="yearChangeTo(this.value)">
									<%
										for (int i = 15; i > 0; i--) { // 抓取15年
									%>
									<option value="<%=year2 - i%>"><%=year2 - i%>年
									</option>
									<%
										}
									%>
									<option selected value="<%=year2 %>"><%=year2 %>年
									</option>
								</select> <select class="col custom-select  form-control-sm"
									name="monthto" id="monthListTo"
									onchange="monthChangeTo(this.value)">
									<%
										for (int i = 0; i <= month2; i++) {
											if (i != month2) {
									%>
									<option value="<%=i%>"><%=i+1%>月
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i+1%>月
									</option>
									<%
										}
										}
									%>
								</select> <select class="col custom-select form-control-sm" name="dayto"
									id="dayListTo">
									<%
										for (int i = 1; i <= day2; i++) {
											if (i != day2) {
									%>
									<option value="<%=i%>"><%=i%>日
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i%>日
									</option>
									<%
										}
										}
									%>
								</select>
							</div>
						</th>
						<th scope="col" class="text-right"><button type="submit"
								class="btn btn-secondary">送出</button></th>
					</tr>
				</thead>
			</table>
			</form>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<form action="../../report_general" method="post">
					<table>
						<tr>
							<td class="align-middle">
							<input type="hidden" name="days" value="7">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									7日內</button></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="col-lg-4">
				<form action="../../report_general" method="post">
					<table>
						<tr>
							<td class="align-middle"><input type="hidden"
								name="days" value="15">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									15日內</button></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="col-lg-4">
				<form action="../../report_general" method="post">
					<table>
						<tr>
							<td class="align-middle"><input type="hidden"
								name="days" value="30">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									30日內</button></td>
						</tr>
					</table>
				</form>
			</div>
		</div>

		<br>

		<div class="row">

			<div class="col-lg-12">
				<div class="card border-info mb-3">
					<div class="card-body text-dark">
						<div class="row">
<%if(rbo!=null) {%>
							<div class="col-lg-6">
					<div>外送訂單筆數：<%=rbo.getDev_count() %></div>
					<div>外送訂單金額：<%=rbo.getDev_sum() %></div>
					<div>門市訂單筆數：<%=rbo.getStore_count() %></div>
					<div>門市訂單金額：<%=rbo.getStore_sum() %></div>
					<div>合計訂單總額：<%=rbo.getTotal_sum() %></div>

							</div>
							<div class="col-lg-6">
					<div>定單取消筆數：<%=rbo.getBad_count() %></div>
					<div>定單取消金額：<%=rbo.getBad_sum() %></div>
					<div>銷售第一名:<%=rbo.getFst_name() %>/<%=rbo.getFst_qty() %>(杯)</div>
					<div>銷售第二名:<%=rbo.getSec_name() %>/<%=rbo.getSec_qty() %>(杯)</div>
					<div>銷售第三名:<%=rbo.getTrd_name() %>/<%=rbo.getTrd_qty() %>(杯)</div>
							</div>
<%}else{ %>
					<div class="col-lg-6">
					<div>外送訂單筆數：0</div>
					<div>外送訂單金額：0</div>
					<div>門市訂單筆數：0</div>
					<div>門市訂單金額：0</div>
					<div>合計訂單總額：0</div>
				
					</div>
					<div class="col-lg-6">
					<div>定單取消筆數：0</div>
					<div>定單取消金額：0</div>
					<div>銷售第一名:NULL / 0 (杯)</div>
					<div>銷售第二名:NULL / 0 (杯)</div>
					<div>銷售第三名:NULL / 0 (杯)</div>
					</div>
<%} %>
						</div>
					</div>

				</div>
			</div>
		</div>





<!--  
		<table class="table table-bordered  table-striped text-center ">
			<thead class="thead-dark">
				<tr>
					<th scope="col"><small>訂單時間</small></th>
					<th scope="col"><small>訂單編號</small></th>
					<th scope="col"><small>訂單類別</small></th>
					
					
					<th scope="col"><small>訂單金額</small></th>
					<th scope="col"><small>首購客戶</small></th>
					
					<th scope="col"><small>訂單狀態</small></th>
					
					<th scope="col" width="20%"><small>訂單註記</small></th>


				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>

				</tr>


				<tr>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>

				</tr>
			</tbody>
		</table>
-->
	</div>

</body>
</html>