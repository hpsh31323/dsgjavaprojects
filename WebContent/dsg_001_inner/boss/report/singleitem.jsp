<%@page import="java.text.DecimalFormat"%>
<%@page import="dsg.Report_By_Product"%>
<%@page import="dsg.Report_By_Order"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%
	Calendar calendar2 = Calendar.getInstance();
	int s_year1 = (session.getAttribute("year1")!=null)?Integer.parseInt((String)session.getAttribute("year1")):calendar2.get(Calendar.YEAR);
	int s_month1 = (session.getAttribute("month1")!=null)?Integer.parseInt((String)session.getAttribute("month1")):calendar2.get(Calendar.MONTH);
	int s_day1 = (session.getAttribute("day1")!=null)?Integer.parseInt((String)session.getAttribute("day1")):calendar2.get(Calendar.DAY_OF_MONTH);
	int s_year2 = (session.getAttribute("year2")!=null)?Integer.parseInt((String)session.getAttribute("year2")):calendar2.get(Calendar.YEAR);
	int s_month2 = (session.getAttribute("month2")!=null)?Integer.parseInt((String)session.getAttribute("month2")):calendar2.get(Calendar.MONTH);
	int s_day2 = (session.getAttribute("day2")!=null)?Integer.parseInt((String)session.getAttribute("day2")):calendar2.get(Calendar.DAY_OF_MONTH);
	Report_By_Product [] rbps = (Report_By_Product[])session.getAttribute("report_By_Products");
%>
<script type="text/javascript">
	function onload(y) //y為年份
	{
		var year = parseInt(y);//取得年份   
		var yearList = document.getElementById("yearListFrom");
		for (var i = 0; i < (year - (year - 15)); i++) {
			var addYear = year - i;
			yearList.options[i] = new Option(addYear + "年", addYear);
		}
		yearChangeFrom();

	}

	function yearChangeFrom() {
		var monthList = document.getElementById("monthListFrom");
		monthList.innerHTML = "";
		for (var i = 0; i < 12; i++) {
			monthList.options[i] = new Option(i + 1 + "月", i + 1);
		}
		var dayList = document.getElementById("dayListFrom");
		dayList.innerHTML = "";
		for (var i = 0; i < 31; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}

	function monthChangeFrom(m) {//m為月份  
		var year = parseInt(document.getElementById("yearListFrom").value);
		var day = 31;
		switch (parseInt(m))//判斷月份並對應月份天數  
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = 28;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = 30;
			break;
		}
		if (day == 28)//二月份的情况下  
		{
			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))//判斷是否為閏年  
			{
				day = 29;
			}
		}
		var dayList = document.getElementById("dayListFrom");
		dayList.innerHTML = "";
		for (var i = 0; i < day; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}

	function yearChangeTo(y) {
		var monthList = document.getElementById("monthListTo");
		monthList.innerHTML = "";
		for (var i = 0; i < 12; i++) {
			monthList.options[i] = new Option(i + 1 + "月", i + 1);
		}
		var dayList = document.getElementById("dayListTo");
		dayList.innerHTML = "";
		for (var i = 0; i < 31; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
		onload(y);
	}

	function monthChangeTo(m) {//m為月份  
		var year = parseInt(document.getElementById("yearListTo").value);
		var day = 0;
		switch (parseInt(m))//判斷月份並對應月份天數  
		{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = 28;
			break;
		case 4:
		case 6:
		case 9:

	case 11:
			day = 30;
			break;
		}
		if (day == 28)//二月份的情况下  
		{
			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))//判斷是否為閏年  
			{
				day = 29;
			}
		}
		var dayList = document.getElementById("dayListTo");
		dayList.innerHTML = "";
		for (var i = 0; i < day; i++) {
			dayList.options[i] = new Option(i + 1 + "日", i + 1);
		}
	}
</script>
<head>
<meta charset="BIG5">
<title>Insert title here</title>
</head>

<body style="background-color: #ecedea;">
	<div class="container marketing">
	<div class="py-4 text-center">
			<h5>
				<a class="text-muted"> 請選取時間範圍</a>
			</h5>
			<form action="../../report_singleitem" method="post">
			<table class="table text-center ">
				<thead>
					<tr>
						<th scope="col">
							<div class="form-row">
								<label>起：</label> <select
									class="col custom-select  form-control-sm" id="yearListFrom" name="yearfrom"
									onchange="yearChangeFrom()">
									<%
										for (int i = 15; i > 0; i--) { // 抓取15年
									%>
									<option value="<%=s_year2 - i%>"><%=s_year2 - i%>年
									</option>
									<%
										}
									%>
									<option selected value="<%=s_year1 %>"><%=s_year1 %>年
									</option>
								</select> 
								<select class="col custom-select  form-control-sm" name="monthfrom"
									id="monthListFrom" onchange="monthChangeFrom(this.value)">
									<%
										for (int i = 0; i <= s_month2; i++) {
											if (i != s_month1) {
									%>
									<option value="<%=i%>"><%=i+1%>月
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i+1%>月
									</option>
									<%
										}
										}
									%>
								</select> <select class="col custom-select form-control-sm"
									id="dayListFrom" name="dayfrom">
									<%
										for (int i = 1; i <= s_day2; i++) {
											if (i != s_day1) {
									%>
									<option value="<%=i%>"><%=i%>日
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i%>日
									</option>
									<%
										}
										}
									%>
								</select>
							</div>
						</th>
						<th scope="col"></th>
						<th scope="col">

							<div class="form-row">
								<label>迄：</label> <select
									class="col custom-select  form-control-sm" name="yearto"
									id="yearListTo" onchange="yearChangeTo(this.value)">
									<%
										for (int i = 15; i > 0; i--) { // 抓取15年
									%>
									<option value="<%=s_year2 - i%>"><%=s_year2 - i%>年
									</option>
									<%
										}
									%>
									<option selected value="<%=s_year2 %>"><%=s_year2 %>年
									</option>
								</select> <select class="col custom-select  form-control-sm"
									name="monthto" id="monthListTo"
									onchange="monthChangeTo(this.value)">
									<%
										for (int i = 0; i <= 12; i++) {
											if (i != s_month2) {
									%>
									<option value="<%=i%>"><%=i+1%>月
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i+1%>月
									</option>
									<%
										}
										}
									%>
								</select> <select class="col custom-select form-control-sm" name="dayto"
									id="dayListTo">
									<%
										for (int i = 1; i <= s_day2; i++) {
											if (i != s_day2) {
									%>
									<option value="<%=i%>"><%=i%>日
									</option>
									<%
										} else {
									%>
									<option value="<%=i%>" selected="selected"><%=i%>日
									</option>
									<%
										}
										}
									%>
								</select>
							</div>
						</th>
						<th scope="col" class="text-right"><button type="submit"
								class="btn btn-secondary">送出</button></th>
					</tr>
				</thead>
			</table>
			</form>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<form action="../../report_singleitem" method="post">
					<table>
						<tr>
							<td class="align-middle">
							<input type="hidden" name="days" value="7">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									7日內</button></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="col-lg-4">
				<form action="../../report_singleitem" method="post">
					<table>
						<tr>
							<td class="align-middle"><input type="hidden"
								name="days" value="15">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									15日內</button></td>
						</tr>
					</table>
				</form>
			</div>
			<div class="col-lg-4">
				<form action="../../report_singleitem" method="post">
					<table>
						<tr>
							<td class="align-middle"><input type="hidden"
								name="days" value="30">
								<button type="submit" class="btn btn-primary btn-lg btn-block">
									30日內</button></td>
						</tr>
					</table>
				</form>
			</div>
		</div>

		<br>








		<table class="table table-bordered  table-striped text-center ">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="15%"><small>品名</small></th>
					<th scope="col" width="10%"><small>外送占比</small></th>
					<th scope="col" width="10%"><small>門市占比</small></th>
					<th scope="col" width="10%"><small>銷售杯數</small></th>
					<th scope="col" width="10%"><small>銷售總額</small></th>
					<th scope="col" width="15%"><small>最佳甜度</small></th>
					<th scope="col" width="15%"><small>最佳溫度</small></th>
					<th scope="col" width="15%"><small>最佳添加</small></th>
				</tr>
			</thead>
			<tbody>
			<%if(rbps!=null){
			for(Report_By_Product p : rbps){ 
			DecimalFormat df = new DecimalFormat("#0.00%");
			int total = p.getDev_qty()+p.getStore_qty();
			int total_sum = p.getDev_sum()+p.getStore_sum();
			double d = p.getDev_qty()*1.0;
			double s = p.getStore_qty()*1.0;
			String dev = df.format(d/(d+s));
			String sto = df.format(s/(d+s));
			%>
				<tr>
					<td class="align-middle"><small><%=p.getP_name() %></small></td>
					<td class="align-middle"><small><%=dev %></small></td>
					<td class="align-middle"><small><%=sto %></small></td>
					<td class="align-middle"><small><%=total %></small></td>
					<td class="align-middle"><small><%=total_sum %></small></td>
					<td class="align-middle"><small><%=p.getBest_s() %></small></td>
					<td class="align-middle"><small><%=p.getBest_t() %></small></td>
					<td class="align-middle"><small><%=p.getBest_a() %></small></td>

				</tr>
<%
} 
}
%>
			</tbody>
		</table>

	</div>

</body>
</html>