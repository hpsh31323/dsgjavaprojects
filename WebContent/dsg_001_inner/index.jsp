<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0 , shrink-to-fit=no"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/floating-labels.css" rel="stylesheet">
<head>
<meta charset="BIG5">

</head>
<%
String errorInfo = (String)request.getAttribute("loginError");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/index.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
<%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/index.jsp' ;  
</script>	
<%
}
%>
<body style="background-color:#eff9ff;">
<form class="form-signin" method="post" action="../employee_login">
      <div class="text-center mb-4">
        <img class="mb-4" src="../img/dsg_logo.png" alt="" width="150" >
        <h1 class="h3 mb-3 font-weight-normal">員工後台工作頁面</h1>
        <p>請登入您個人的帳號與密碼即可進入公共頁面</p><p><a href="logrtv.jsp">忘記密碼請按此取回</a></p>
      </div>

      <div class="form-label-group">
        <input type="text" id="inputid" name="account" class="form-control" placeholder="inser your id" required autofocus>
        <label for="inputid">帳號</label>
      </div>

      <div class="form-label-group">
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <label for="inputPassword">密碼</label>
      </div>
     
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" name="person" value="1"> 進入設定頁面
        </label>
      </div>
      
      <button class="btn btn-lg btn-primary btn-block" type="submit">登入</button>
  
    </form>
  </body>

</html>
