<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="dsg.Order" import="dsg.OrderInfo"
	import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<title>MainPage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/form-validation.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<head>
<%

			Order ord = (Order) request.getSession().getAttribute("ord");
			ArrayList<OrderInfo> oriList = (ArrayList<OrderInfo>) request.getSession().getAttribute("ora");
			//out.println(oriList.get(i).getAdd1_name().equals("無")?"":oriList.get(i).getAdd1_name());
%>
<%@ include file="head.jsp"%>

</head>
<body style="background-color: #eaedea;">
	<form method="post" action="../../oin_orderfinished">
		<!-- 標題   -->
		<div class="container" style="margin-top: 5rem;">
			<div class="py-5 text-center">
				<h2>點餐清單</h2>
			</div>



			<!-- 左邊欄開始,表格形式,很好理解,自適應,不要去設定表格寬  -->
			<div class="row justify-content-center">
				<div class="col-md-9 order-md-1">
					<h4 class="mb-3">商品明細</h4>


					<table class="table table-striped">
						<!-- 表格頭 -->
						<thead>
							<tr>

								<th scope="col"><a class="h6">品項</a></th>
								<th scope="col"><a class="h6">名稱</a></th>
								<th scope="col"><a class="h6">內容</a></th>
								<th scope="col"><a class="h6">容量</a></th>
								<th scope="col"><a class="h6">數量</a></th>
								<th scope="col"><a class="h6">金額</a></th>

								<th scope="col"><a class="h6">修改</a></th>

								<th scope="col"><a class="h6">刪除</a></th>

							</tr>
						</thead>

						<tbody>
							<!-- 產品明細,可依資料庫抓值,先放三個 ,最後修改按鈕連對客製化對話框,在表格下方  -->




							<%
	int sum=0;
	if (!oriList.isEmpty()) {
		for (int i = 0; i < oriList.size(); i++){
			/*out.println("<tr><td>" +(i+1) + 
						"<td>"+oriList.get(i).getPd_name() + 
					"<td>"+oriList.get(i).getTemper()+"/"+oriList.get(i).getSugar()+"/加料:"+(oriList.get(i).getAdd1_name().equals("無")?"無":oriList.get(i).getAdd1_name())+(oriList.get(i).getAdd2_name().equals("無")?"":","+oriList.get(i).getAdd2_name())+(oriList.get(i).getAdd3_name().equals("無")?"":","+oriList.get(i).getAdd3_name())+
					"<td>"+oriList.get(i).getSize()+
					"<td>"+oriList.get(i).getQty()+
					"<td>"+oriList.get(i).getItem_price()+
					"<td>"+"<input type=button value=修改 onclick=location.href='order_edit.jsp?id="+i+"'>"+
					"<td>"+"<input type=button value=刪除 onclick=location.href='../../oin_delete?id="+i+"'>"
					);*/
		
			%>
							<tr>
								<th scope="col"><small><%=i+1 %></small></th>
								<th scope="col"><small><%=oriList.get(i).getPd_name() %></small></th>
								<th scope="col"><small><%=oriList.get(i).getSugar() %>/<%=oriList.get(i).getTemper() %>/<%=(oriList.get(i).getAdd1_name().equals("無")?"":"/"+oriList.get(i).getAdd1_name()) %><%=(oriList.get(i).getAdd2_name().equals("無")?"":"/" +oriList.get(i).getAdd2_name()) %><%=(oriList.get(i).getAdd3_name().equals("無")?"":"/"+oriList.get(i).getAdd3_name()) %></small></th>
								<th scope="col"><small>
										<%
	      if(oriList.get(i).getSize()==1)
	      {
	    	  out.print("大杯");
	      }else{
	    	  out.print("中杯");
	      }

	       %>
								</small></th>
								<th scope="col"><small><%=oriList.get(i).getQty() %></small></th>
								<th scope="col"><small><%=oriList.get(i).getItem_price() %></small></th>
								<!--  
	      <th scope="col"><a class="btn btn-outline-info btn-sm" data-toggle="modal" href="#modalopt" role="button"><small >修改</small></a></th>
	      -->
								<th scope="col"><a class="btn btn-outline-secondary btn-sm"
									href="order_edit.jsp?id=<%=i %>" role="button"><small>修改</small></a></th>
								<th scope="col"><a class="btn btn-outline-secondary btn-sm"
									href="../../oin_delete?id=<%=i %>" role="button"><small>刪除</small></a></th>
							</tr>

							<% 
	sum+=oriList.get(i).getItem_price();
	}
		ord.setSum(sum);
   }
   
   %>

							<tr>
								<th scope="col" width="10%">合計</th>
								<th scope="col"><%=ord.getSum()%></th>
							</tr>
						</tbody>
					</table>






					<!--  這裏開始是設條件式,依客戶的狀況顯示 -->
					<!--  會員點數使用  -->
					<!--  
<table class="table table-striped">
  <thead >
    <tr>   
      <th scope="col" width="15%"><label>點數扺用</label></th>
      <th scope="col" >
  <div class="form-row align-items-center">
     <div class="col-auto my-1">
     
      <input type="text" class="form-control" id="inlineFormInputName" placeholder="折抵上限36點">
    </div>
    <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="autoSizingCheck2">
        <label class="form-check-label" >
          全額扺用
        </label>
      </div>
    </div>   
    <div class="col-auto my-1">
      <button type="submit" class="btn btn-primary btn-sm"><small>確定</small></button>
    </div>
  </div>
</th>
   </tr>
  </thead>

</table>
-->
					<!--  統一編號  -->
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col" width="15%"><label>統一編號</label></th>
								<th scope="col">
									<div class="form-row align-items-center">
										<div class="col">

											<input type="text" class="form-control" id="O_taxid"
												name="taxid" placeholder="沒有可不輸入" value="">

										</div>


									</div>
								</th>
							</tr>
						</thead>

					</table>

					<!--  改變訂單型態  -->
					<!--  
<table class="table table-striped">
  <thead >
    <tr>   
      <th scope="col" width="15%"><label>配送方式</label></th>
      <th scope="col" >
  <div class="form-row align-items-center">
    <div class="col">
   //程式碼
    if(ord.getDeliver()==0)
    {
    	out.print("<label><a class='text-primary'>自行取餐</a></label>");
    	
    }else{
    	out.print("<label><a class='text-primary'>外送</a></label>");
    }
    
    
    
      
     
    </div>
    
   
  </div>
</th>
   </tr>
  </thead>

</table>
-->
					<!--  改配送地址  -->
					<!--  
<table class="table table-striped">
  <thead >
    <tr>   
      <th scope="col" width="15%"><label>配送地點</label></th>
      <th scope="col">
  <div class="form-row align-items-center">
    <div class="col my-1">
      <label class="sr-only" for="inlineFormInputName">Name</label>
      <input type="text" class="form-control" id="inlineFormInputName" placeholder="地址" value="<%=ord.getAdd()%>" readonly="readonly">
    </div>
 
  </div>

</th>
   </tr>
  </thead>

</table>
-->
					<hr class="mb-4">
					<div class="row justify-content-center">
						<div class="col-5">
							<button class="btn btn-primary btn-lg btn-block" type="submit">確定送出</button>
						</div>
						<div class="col-5">
							<input type=button class="btn btn-primary btn-lg btn-block"
								onclick="location.href='order.jsp?class_id=1'" value="回上一頁">
						</div>
					</div>
				</div>
			</div>

		</div>
		</div>
	</form>


</body>
<%@ include file="footer.jsp" %>
</html>