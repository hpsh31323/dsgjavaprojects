<%@page import="java.text.DecimalFormat"%>
<%@page import="dsg.OrderInfo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="dsg.Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>
<title>MainPage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<head>
<%@ include file="head.jsp"%>
</head>
<%response.setHeader("refresh","30;URL=../../ready_order"); %>
<body style="background-color: #ecedea;">
	<div class="container marketing" style="margin-top: 5rem;">





		<div class="row">

			<!-- 左邊欄開始  -->
			<div class="col-md-5 order-md-1">
				<div class="py-3 text-center">
					<h2>待取客戶</h2>
				</div>
				<hr>
				<!-- 迴圈開始 -->
<%
Order[] o_self = (Order [])session.getAttribute("o_self");
if(o_self!=null){
	for(Order o : o_self){ 
SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
	String time = sdf.format(o.getDate()); 
	DecimalFormat df = new DecimalFormat("000");
	String rank = df.format(o.getRank());
	%>
				<div class="row text-center"
					style="box-shadow: inset 1px -1px 1px, inset -1px 1px 1px; padding: 4px 2px 2px;">
					<div class="row  text-center">
						<div class="col-md-3">
							<h3>
								<strong><%=rank %></strong>
							</h3>
						</div>
						<div class="col-md-3">
							<h3><%=o.getQty() %>(cup)</h3>
						</div>
						<div class="col-md-4">
							<h3>NT <%=o.getSum() %></h3>
						</div>
						<div class="col-md-2">
							<h3><%=time %></h3>
						</div>

					</div>
					<div class="row mx-auto">
						
						<div class="col-md-4 ">
						<form action="../../order_set_finish" method="post">
						<input type="hidden" name="path" value="ready_order">
							<button type="submit" class="btn btn-primary" name="item" value="<%=o.getO_id() %>">取餐</button>
						</form>
						</div>
							
						
						<div class="col-md-4">
						<form action="../../order_set_bad_debts" method="post">
						<input type="hidden" name="path" value="ready_order">
							<button type="submit" class="btn  btn-outline-danger " name="item" value="<%=o.getO_id() %>">未取</button>
						</form>
						</div>
						
						<div class="col-md-4 ">
							<button type="button" class="btn btn-outline-info "
								data-toggle="modal" data-target="#detail<%=o.getO_id() %>">明細</button>
						</div>
					</div>
				
				</div>




				<!-- Modal -->
				<div class="modal fade" id="detail<%=o.getO_id() %>" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">單號：<%=rank %></h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">


								<div>訂購時間：<%=time %></div>
								<div>品項：</div>
								<%for(OrderInfo info : o.getItem()){ 
								String add1 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String add2 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String add3 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String size = (info.getSize()==1)?"大杯":"中杯";
								%>
								<div>
									<%=info.getPd_name() %>(<%=size %>/<%=info.getTemper() %>/<%=info.getSugar() %><%=add1 %><%=add2 %><%=add3 %>)*<%=info.getQty() %><br>
								</div>
								<%} %>






							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
<%
				}
			}
%>
				<!-- 迴圈結束 -->
			</div>
			
			<!--左邊欄結束  -->
			
			<div class="col-md-1 order-md-2"></div>

			<!--右邊欄開始  -->
			<div class="col-md-6 order-md-3">
				<div class="py-3 text-center">
					<h2>待送客戶</h2>
				</div>
				<hr>
				
				<!-- 迴圈開始 -->
<%
Order[] o_dev = (Order [])session.getAttribute("o_dev");
if(o_dev!=null){
	for(Order o : o_dev){ 
SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
	String time = sdf.format(o.getDate()); 
	DecimalFormat df = new DecimalFormat("000");
	String rank = df.format(o.getRank());
	%>
				<div class="row text-center"
					style="box-shadow: inset 1px -1px 1px, inset -1px 1px 1px; padding: 4px 2px 2px;">
					<div class="row  text-center">
						<div class="col-md-2">
							<h3>
								<strong><%=rank %></strong>
							</h3>
						</div>
						<div class="col-md-2">
							<h3><%=o.getQty() %>(cup)</h3>
						</div>
						<div class="col-md-3">
							<h3>NT <%=o.getSum() %></h3>
						</div>
						<div class="col-md-5">
							<h6><%=o.getAdd() %></h6>
						</div>

					</div>
					<div class="row mx-auto">
					
						<div class="col-md-4 ">
						<form action="../../order_set_on_dev" method="post">
							<button type="submit" class="btn btn-primary" name="item" value="<%=o.getO_id() %>">外送</button>
							</form>
						</div>
						
						
						<div class="col-md-4">
						<form action="../../order_set_bad_debts" method="post">
						<input type="hidden" name="path" value="ready_order">
							<button type="submit" class="btn btn-outline-danger " name="item" value="<%=o.getO_id() %>">取消</button>
						</form>
						</div>
						
						<div class="col-md-4 ">
							<button type="button" class="btn btn-outline-info "
								data-toggle="modal" data-target="#detail<%=o.getO_id() %>">明細</button>
						</div>


					</div>






				</div>
				
			<!-- Modal -->
				<div class="modal fade" id="detail<%=o.getO_id() %>" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">單號：<%=rank %></h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">


								<div>訂購時間：<%=time %></div>
								<div>外送地址：<%=o.getAdd() %></div>
								<div>品項：</div>
								<%for(OrderInfo info : o.getItem()){ 
									String add1 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add2 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add3 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String size = (info.getSize()==1)?"大杯":"中杯";
								%>
								<div>
									<%=info.getPd_name() %>(<%=size %>/<%=info.getTemper() %>/<%=info.getSugar() %><%=add1 %><%=add2 %><%=add3 %>)*<%=info.getQty() %><br>
								</div>
								<%} %>






							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				
				
<%
				}
			}
%>
				<!-- 迴圈結束 -->
				<!-- 迴圈開始 -->
<%
Order[] o_ondev = (Order [])session.getAttribute("o_ondev");
if(o_ondev!=null){
	for(Order o : o_ondev){ 
SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
	String time = sdf.format(o.getDate()); 
	DecimalFormat df = new DecimalFormat("000");
	String rank = df.format(o.getRank());
	%>
				<div class="row text-center"
					style="box-shadow: inset 1px -1px 1px, inset -1px 1px 1px; padding: 4px 2px 2px;">
					<div class="row  text-center">
						<div class="col col-md-2">
							<h3>
								<strong><%=rank %></strong>
							</h3>
						</div>
						<div class="col col-md-2">
							<h3><%=o.getQty() %>(cup)</h3>
						</div>
						<div class="col col-md-3">
							<h3>NT <%=o.getSum() %></h3>
						</div>
						<div class="col col-md-5">
							<h5>=====外送中=====</h5>
						</div>

					</div>
					<div class="row mx-auto">
					
						<div class="col-md-4 ">
						<form action="../../order_set_finish" method="post">
						<input type="hidden" name="path" value="ready_order">
							<button type="submit" class="btn btn-primary" name="item" value="<%=o.getO_id() %>">結單</button>
							</form>
						</div>
						
						
						<div class="col-md-4">
						<form action="../../order_set_bad_debts" method="post">
						<input type="hidden" name="path" value="ready_order">
							<button type="submit" class="btn btn-outline-danger " name="item" value="<%=o.getO_id() %>">棄單</button>
						</form>
						</div>
						
						<div class="col-md-4 ">
							<button type="button" class="btn btn-outline-info "
								data-toggle="modal" data-target="#detail<%=o.getO_id() %>">明細</button>
						</div>


					</div>






				</div>
				
			<!-- Modal -->
				<div class="modal fade" id="detail<%=o.getO_id() %>" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">單號：<%=rank %></h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">


								<div>訂購時間：<%=time %></div>
								<div>外送地址：<%=o.getAdd() %></div>
								<div>品項：</div>
								<%for(OrderInfo info : o.getItem()){ 
									String add1 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add2 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add3 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String size = (info.getSize()==1)?"大杯":"中杯";
								%>
								<div>
									<%=info.getPd_name() %>(<%=size %>/<%=info.getTemper() %>/<%=info.getSugar() %><%=add1 %><%=add2 %><%=add3 %>)*<%=info.getQty() %><br>
								</div>
								<%} %>






							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				
				
<%
				}
			}
%>
				<!-- 迴圈結束 -->
			</div>
		
		<!--右邊欄結束  -->
		
		
		</div>
	</div>

</body>
</html>