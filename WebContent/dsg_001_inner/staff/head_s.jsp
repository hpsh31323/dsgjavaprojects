<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!--這個檔案是所有頁面頭上的那條黑色選單 -->

<html>
<title>Insert title here</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
            <link href="../../css/bootstrap.min.css" rel="stylesheet">   
            <script src="https://code.jquery.com/jquery.js"></script>
            <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
            <script src="../../js/bootstrap.min.js"></script>
       
<body>
<nav class="navbar navbar-dark  navbar-expand-md  fixed-top" style="background-color:#5b3d20;">
<!-- 圖片的位置和連結,目前是連到首頁 -->
    <a class="navbar-brand" href="../index.jsp"><img src="../../img/dsg_logo.png" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
 
       <li class="nav-item">
           <a class="nav-link" href="manage.jsp" >修改資料</a>     
        </li>
        
             <li class="nav-item">
           <a class="nav-link" href="../../deliverlist">外送列表</a>     
        </li>
        

 
    
      </ul>   
 &nbsp;
			<button class="btn btn-outline-info " type="button"
				type="button" data-toggle="modal" data-target="#logout">
				<img src="../../img/logout.png" height="25">
			</button>
			 </div>     

 </nav>
 
 						<!-- Modal -->
				<div class="modal fade" id="logout" tabindex="-1" role="dialog"
					aria-labelledby="exampleModalLabel" aria-hidden="false">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">登出</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">


								<div>確認登出</div>


							</div>
							<div class="modal-footer">
							<form action="../../inner_logout" method="post">
							<input type="hidden" name="path" value="dsg_001_inner/index.jsp">
								<button type="submit" class="btn btn-danger"
									>登出</button>
									</form>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal">取消</button>
							</div>
						</div>
					</div>
				</div>

</body>
</html>