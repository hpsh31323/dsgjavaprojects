<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<!--  客製化選單   -->
<html>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">    
      <link href="../css/form-validation.css" rel="stylesheet"> 
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
           
   <head>

  </head>
 <body>
 
       <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">風味選項</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      
    <form>  
<table class="table table-striped">
   <tbody >
    <tr>   
      <th scope="col" width="15%"> <div class="form-row align-items-center"> <label>容量</label></div></th>
      <th scope="col">
  <div class="form-row align-items-center">  
    <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt1" name="opt1">
        <label >
          500 C.C.
        </label>
      </div>
    </div>   
    <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt1" name="opt1">
        <label>
          700 C.C.
        </label>
      </div>    </div>
  </div>
</th>
   </tr>
  </tbody>
   <tbody >
    <tr>   
      <th scope="col"> <div class="form-row align-items-center"> <label>溫度</label></div></th>
      <th scope="col">
  <div class="form-row align-items-center">  
    <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2">
        <label ><a class="text-danger">熱飲</a></label>
      </div>
    </div>   
    <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2">
        <label> 常溫</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2">
        <label> 去冰</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2">
        <label> 微冰</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2">
        <label> 少冰</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt2" name="opt2" checked>
        <label> 正常</label>
      </div>
      </div>
     
  </div>
</th>
   </tr>
  </tbody>
   <tbody >
    <tr>   
      <th scope="col"> <div class="form-row align-items-center"> <label>甜度</label></div></th>
      <th scope="col">
  <div class="form-row align-items-center">  
    <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt3" name="opt3">
        <label >倍糖</label>
      </div>
    </div>   
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt3" name="opt3" checked>
        <label> 正常</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt3" name="opt3">
        <label> 少糖</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt3" name="opt3">
        <label> 微糖</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt3" name="opt3">
        <label> 無糖</label>
      </div>
      </div>
     
  </div>
</th>
   </tr>
  </tbody>
   <tbody >
    <tr>   
      <th scope="col"> <div class="form-row align-items-center"> <label>加味</label></div></th>
      <th scope="col">
  <div class="form-row align-items-center">  
   <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="optck1">
        <label class="form-check-label" >
          仙草凍
        </label>
      </div>
    </div>   
       <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="optck2">
        <label class="form-check-label" >
          大珍珠
        </label>
      </div>
    </div>   
       <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="optck3">
        <label class="form-check-label" >
          山粉圓
        </label>
      </div>
    </div>   
       <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="optck4">
        <label class="form-check-label" >
          大紅豆
        </label>
      </div>
    </div>   
      
  </div>
</th>
   </tr>
  </tbody>
  
  
  <tbody >
    <tr>   
      <th scope="col"><label>同款杯數</label></th>
      <th scope="col">
  <div class="form-row align-items-center">
      <div class="col-auto my-1">
      <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt4" name="opt4">
        <label >一杯</label>
      </div>
    </div>   
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt4" name="opt4" checked>
        <label>二杯</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt4" name="opt4">
        <label>三杯</label>
      </div>
      </div>
       <div class="col-auto my-1">
     <div class="form-check">
        <input class="form-check-radio" type="radio" id="opt4" name="opt4">
        <label>三杯以上</label>
      </div>
      </div>
 
    <div class="col-auto my-1">  
      <input type="text" class="form-control" placeholder="５">
    </div> 
    </div> 
</th>
   </tr>
  </tbody>
 
</table>





      <div class="modal-footer" >
           <button type="button" class="btn btn-primary">完成</button>
      </div>
      </form>         
   
 


       

 
   </body>
</html>