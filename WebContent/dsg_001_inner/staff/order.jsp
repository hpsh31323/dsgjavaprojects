<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="dsg_dbhelper.DB_Helper"
	import="java.sql.*" import="dsg.Order" import="dsg.OrderInfo"
	import="java.util.ArrayList"%>
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>
<title>MainPage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/carousel.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<head>
<%@ include file="head.jsp"%>
<%
	ArrayList<OrderInfo> oriList = (ArrayList<OrderInfo>) session.getAttribute("ora");
	Order ord = (Order) session.getAttribute("ord");
	ArrayList<String> pd_id_list = (ArrayList<String>) session.getAttribute("pd_id_list");
	ArrayList<String> pd_name = (ArrayList<String>) session.getAttribute("pd_name");
	ArrayList<String> pd_class = (ArrayList<String>) session.getAttribute("pd_class");
%>

<meta charset="BIG5">
<title>DSG訂單系統</title>


<%
	/*out.println("購買清單:" + "<br>");
	if (!oriList.isEmpty()) {
		for (int i = 0; i < oriList.size(); i++)
			out.println(oriList.get(i).getPd_name() + "	" + oriList.get(i).getQty() + "杯<br>");
	}
	out.println("目前總額:" + ord.getSum());*/
%>
</head>
<body>
	<div class="container marketing" style="margin-top: 5rem;">
		<h3 align=center>歡迎光臨DSG</h3>
		<div class="row deliver align-items-center"
			style="padding: 4px 2px 2px;">
			<%
				for (int i = 0; i < pd_class.size(); i++) {
					%><div class='col-md-3'>
							<button type='button' class='btn btn-outline-success btn-lg btn-block' onclick=location.href='../../order?class_id=<%=(i+1)%>'> 
							<%=pd_class.get(i) %> 
							</button>
							<p>
							</p>
							</div>
				
				<% 
				}
			%>
		</div>
		<hr class="deliver-divider">
			<table border=1 align=center>
				<tr>
					<td colspan=5 align=center>商品項目
				<tr>
					<%
						int count = 0;
						for (int i = 0; i < pd_name.size(); i++) { 
						%>	
							<td>
							<form action="../../order_infoinput" method="post">
							<input type="hidden" name="pd_idd" value="<%=pd_id_list.get(i) %>" >
							<input type='submit' name='pd_name'  style="width:160px;height:160px;" value="<%=pd_name.get(i) %>">
							</form>
							

						<%
						count++;
						if (count == 5) {
							%>
							<tr>
					<%
					count = 0;
					}
						
						} 
						%>
						
				
				<tr>
				
					<td colspan=5 align=center>
					<form method=post action="order_checklist.jsp">
					<input type=submit value="完成點單">
					</form>
			</table>
		
		<hr class="deliver-divider">
	</div>

</body>
<%@ include file="footer.jsp"%>
</html>