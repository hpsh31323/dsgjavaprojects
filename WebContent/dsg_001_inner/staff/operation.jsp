<%@page import="dsg.OrderInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
<title>MainPage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">
<script src="../js/bootstrap.min.js"></script>

<head>
<%@ include file="head.jsp"%>
</head>

<body style="background-color: #ecedea;">

	<div class="container marketing" style="margin-top: 5rem;">
	<form action="../../item_finish" method="post">
		<table class="table table-bordered  table-striped text-center ">
			<thead>
				<tr>
					<th scope="col" width="">單號</th>
					<th scope="col" width="">品名</th>
					<th scope="col" width="">大小</th>
					<th scope="col" width="">數量</th>
					<th scope="col" width="">溫度</th>
					<th scope="col" width="">甜度</th>
					<th scope="col" width="">加料1</th>
					<th scope="col" width="">加料2</th>
					<th scope="col" width="">加料3</th>
					<th scope="col" width="">完成</th>

				</tr>
			</thead>
			<tbody>
			<%
OrderInfo [] infos = (OrderInfo [])session.getAttribute("infos");
for(OrderInfo o : infos){
	String x =(o.getSize()==1)?"大杯":"小杯";
	%>
				<tr>
					<td class="align-middle"><%=o.getO_id() %></td>
					<td class="align-middle"><%=o.getPd_name() %></td>
					<td class="align-middle"><%=x %></td>
					<td class="align-middle"><%=o.getQty() %></td>
					<td class="align-middle"><%=o.getTemper() %></td>
					<td class="align-middle"><%=o.getSugar() %></td>
					<td class="align-middle"><%=o.getAdd1_name() %></td>
					<td class="align-middle"><%=o.getAdd2_name() %></td>
					<td class="align-middle"><%=o.getAdd3_name() %></td>
					<td class="align-middle">
						<button type="submit" class="btn btn-primary" name="item" value="<%=o.getI_id() %>">
							完成
						</button>
					</td>
				</tr>
<% 
}
response.setHeader("refresh","30;URL=../../waiting_list");
%>
			</tbody>
		</table>
	</form>
	</div>

</body>
</html>