<%@page import="dsg.Employee"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 店員管理自己的資料  -->
<html>
 <%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/staff/manage.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/staff/manage.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
  <%@ include file="head_s.jsp"%>
  </head>

<%
Employee e = (Employee)session.getAttribute("employee");
%> 
<body style="background-color:#ecedea;">

<div class="container marketing" style="margin-top:5rem;">
  <div class="row">  
      
      
      <div class="col-lg-3"></div>
      
      
     <div class="col-lg-6">
  
          <form method="post" action="../../staff_manage">
               <div class="text-center">
                 <h5>修改個人資料</h5>
               </div>
                <!--  
                 <label for="password">再輸入一次新密碼</label>
                <input type="password" class="form-control" id="password" placeholder="" value="" required>
               -->

  
                <label for="name">中文姓名</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="<%=e.getName() %>" required>
                
                <label for="password">新密碼</label>
                <input type="password" class="form-control" id="password2" name="password2" placeholder="" >
              
                <label for="phone">行動電話</label>
                <input type="text" class="form-control" id="phone" placeholder="" name="phone" value="<%=e.getTel() %>" required>

                <label for="mail">電子郵件</label>
                <input type="email" class="form-control" id="mail" placeholder="" name="mail" value="<%=e.getMail() %>" required>
                
                <label for="id">身份確認，請輸入原密碼</label>
                <input type="password" class="form-control" id="password1" name="password1" placeholder="" value="" required>
             
           
        
      
          

       <br>
            <button class="btn btn-primary btn-lg btn-block" type="submit">確認送出</button>
         </form>
 </div>
  <div class="col-lg-3"></div>
 </div> </div>

   </body>
   <footer> <%@ include file="footer.jsp"%></footer>
</html>