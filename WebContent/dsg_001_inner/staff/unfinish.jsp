<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="dsg.Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
<title>MainPage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">
<script src="../js/bootstrap.min.js"></script>

<head>
<%@ include file="head.jsp"%>
</head>
<%response.setHeader("refresh","30;URL=../../ready_order"); %>
<body style="background-color: #ecedea;">

	<div class="container marketing" style="margin-top: 5rem;">
		<table class="table table-bordered  table-striped text-center ">
			<thead>
				<tr>
					<th scope="col" width="">訂購時間</th>
					<th scope="col" width="">單號</th>
					<th scope="col" width="">類別</th><!-- 自取外送 -->
					<th scope="col" width="">數量</th>
					<th scope="col" width="">金額</th>
					<th scope="col" width="">客戶</th>
					<th scope="col" width="">電話</th>
					<th scope="col" width="20%">註記</th>
					<th scope="col" width="">放棄</th>


				</tr>
			</thead>
			<tbody>
<%

Order[] o_bad = (Order [])session.getAttribute("o_bad");
if(o_bad!=null){
	for(Order o : o_bad){ 
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	String time = sdf.format(o.getDate()); 
	DecimalFormat df = new DecimalFormat("000");
	String rank = df.format(o.getRank());
	String dev = (o.getDeliver()==0)?"自取":"外送";
	%>
				<tr>
					<td class="align-middle"><%=time %></td>
					<td class="align-middle"><%=rank %></td>
					<td class="align-middle"><%=dev %></td>
					<td class="align-middle"><%=o.getQty() %></td>
					<td class="align-middle"><%=o.getSum() %></td>
					<td class="align-middle"><%=o.getC_name() %></td>
					<td class="align-middle"><%=o.getC_tel() %></td>
				<form action="../../bad_debts" method="post">
				<td class="align-middle">
        <textarea class="form-control" id="Textarea" rows="1" name="note"></textarea></td>
					<td class="align-middle">
					
						<button type="submit" class="btn btn-primary" name="item" value="<%=o.getO_id() %>">
							放棄
						</button>
						
					</td>
					</form>
				</tr>
<%
				}
			}
%>	
			</tbody>
		</table>

	</div>

</body>
</html>