<%@page import="dsg.OrderInfo"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="dsg.Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- 外送員查詢客戶資料  -->
<html>
     <%
String message = (String)request.getAttribute("message");
if(message != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=message%>");  

window.location='dsg_001_inner/staff/deliverlist.jsp' ;  
</script>	
<%
}
%>
<%
String errorInfo = (String)request.getAttribute("Error");
if(errorInfo != null) {
%>
<script type="text/javascript" language="javascript">
alert("<%=errorInfo%>");  

window.location='dsg_001_inner/staff/deliverlist.jsp' ;        // 跳轉登入頁
</script>	
<%
}
%>
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
    
             
   <head>
  <%@ include file="head_s.jsp"%>
  </head>

<body style="background-color:#ecedea;">
<div class="container marketing" style="margin-top:5rem;">
<!-- 迴圈開始 -->
<%
Order[] o_ondev = (Order [])session.getAttribute("o_ondev");
if(o_ondev!=null){
	for(Order o : o_ondev){
SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
	String time = sdf.format(o.getDate()); 
	DecimalFormat df = new DecimalFormat("0000");
	String rank = df.format(o.getRank());
	%>

<!-- 第一筆 -->       
   <hr class="deliver-divider">
    <div class="row deliver align-items-center"  style="box-shadow:inset 1px -1px 1px  , inset -1px 1px 1px  ; padding: 4px 2px 2px;">
  
      <div class="col-md-2"> <h4><%=o.getC_name() %></h4>   <a class="text-muted">(<%=rank %>)</a>
       
      </div>
        <div class="col-md-5 " >
        <h5 ><strong><%=o.getC_tel() %></strong></h5>
         <p class="lead"><%=o.getAdd() %></p>
       </div>
    <div class="col-md-1">
        <h5 class="deliver-heading">杯數</h5>
        <p class="lead"><%=o.getQty() %></p>
      </div>
          <div class="col-md-1">
        <h5 class="deliver-heading">總價</h5>
        <p class="lead"><%=o.getSum() %></p>
      </div>
       <div class="col-md-1">    
       <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#detail<%=o.getRank() %>">明細</button>
      </div>
      <div class="col-md-1 "> 
      <form action="../../order_set_finish" method="post">
      <input type="hidden" name="path" value="deliverlist">
          <button type="submit" class="btn btn-outline-primary" name="item" value="<%=o.getO_id() %>">完成</button>
      </form>
      </div>
      <div class="col-md-1 "> 
      <form action="../../order_set_bad_debts" method="post">
      <input type="hidden" name="path" value="deliverlist">
          <button type="submit" class="btn btn-outline-primary" name="item" value="<%=o.getO_id() %>">失聯</button>
      </form>
      </div>
        
            </div>
            
                    <!-- Modal -->
<div class="modal fade" id="detail<%=o.getRank() %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">單號：<%=rank %></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
         
         <div>訂購時間：<%=time %></div>
         <div>品項：</div>
								<%for(OrderInfo info : o.getItem()){ 
									String add1 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add2 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
									String add3 = (info.getAdd1_name().equals("無"))?"":"/加"+info.getAdd1_name();
								String size = (info.getSize()==1)?"大杯":"中杯";
								%>
								<div>
									<%=info.getPd_name() %>(<%=size %>/<%=info.getTemper() %>/<%=info.getSugar() %><%=add1 %><%=add2 %><%=add3 %>)*<%=info.getQty() %><br>
								</div>
								<%} %>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<%
				}
			}
%>
				<!-- 迴圈結束 -->
  

            
            


  <!-- /END THE FEATURETTES -->
    </div>   

  



  
 
   </body>
   <footer> <%@ include file="footer.jsp"%></footer>
</html>