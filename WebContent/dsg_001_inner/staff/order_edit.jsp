<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="dsg.Order" import="dsg.OrderInfo"
	import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">

<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>

<head>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/carousel.css" rel="stylesheet">
<meta charset="BIG5">
<title>Insert title here</title>
<%@ include file="head.jsp"%>
<%
	Order ord = (Order) session.getAttribute("ord");
	int x = Integer.parseInt(request.getParameter("id"));
	ArrayList<OrderInfo> oriList = (ArrayList<OrderInfo>) session.getAttribute("ora");
%>
</head>
<body style="background-color: #eaedea;">
	<div class="container marketing" style="margin-top: 5rem;">
		<div class="modal-header">
			<h5 class="modal-title" id="ModalLabel">

				<%=oriList.get(x).getPd_name() %></h5>
		</div>
		<hr class="deliver-divider">
		<form method="post" action="../../oin_editOri">
			<table class="table table-striped">
				<input type="hidden" name=pd_id
					value=<%=x%>>
				<tbody>
					<th scope="col" width="15%">
						<div class="form-row align-items-center">
							<label>容量</label>
						</div>
					</th>
					<th scope="col">
						<div class="form-row align-items-center">
							<div class="col-auto my-1">
								<div class="form-check">
									<input class="form-check-radio" type="radio" id="opt1"
										name="size" value="1" checked> <label> 700
										C.C. </label>
								</div>
							</div>
							<div class="col-auto my-1">
								<div class="form-check">
									<input class="form-check-radio" type="radio" id="opt1"
										name="size" value="0"> <label> 500 C.C. </label>
								</div>
							</div>
						</div>
					</th>
					
				</tbody>
			
				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>溫度</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="6"> <label><a
											class="text-danger">熱飲</a></label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="5"> <label> 常溫</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="4"> <label> 去冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="3"> <label> 微冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="2"> <label> 少冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="1" checked> <label> 正常</label>
									</div>
								</div>

							</div>
						</th>
					</tr>
				</tbody>
				
				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>甜度</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="5"> <label>多糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="1" checked> <label> 正常</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="2"> <label> 半糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="3"> <label> 微糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="4"> <label> 無糖</label>
									</div>
								</div>

							</div>
						</th>
					</tr>
				</tbody>
			
				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>加料</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									加料1:<select name="add1">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>
								</div>
								<div class="col-auto my-1">
									加料2:<select name="add2">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>
								</div>
								<div class="col-auto my-1">
									加料3:<select name="add3">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>

								</div>

							</div>
						</th>
					</tr>
				</tbody>
				<tbody>
					<tr>
						<th scope="col"><label>同款杯數</label></th>
						<th scope="col">
							<div class="form-row align-items-center">

								<div class="col-auto my-1">
									<input type="text" class="form-control" name="qty"
										value=<%=oriList.get(x).getQty() %>>
								</div>
							</div>
						</th>
					</tr>
				</tbody>

			</table>




			<hr class="mb-4">
			<div class="row justify-content-center">
				<div class="col-5">

					<button type="submit" class="btn btn-primary btn-lg btn-block">完成</button>
				</div>

				<div class="col-5">
					<button type="button" class="btn btn-primary btn-lg btn-block"
						onclick="javascript:history.back(-1);">回上一頁</button>
				</div>
			</div>
			
		</form>
		<hr class="deliver-divider">
	</div>
	
</body>
<%@ include file="footer.jsp"%>
</html>