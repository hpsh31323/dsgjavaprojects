<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="dsg.Order" import="dsg.OrderInfo"
	import="dsg.Product" import="dsg_dbhelper.Product_DB_Helper"
	import="java.sql.*"%>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/carousel.css" rel="stylesheet">


<head>
<%@ include file="head.jsp"%>
</head>

<body style="background-color: #eaedea;">
	<div class="container marketing" style="margin-top: 5rem;">
		<%
		int x = (int)session.getAttribute("pd_id");
		Product pdt=(Product)session.getAttribute("pdt");
	%>
		<title>Insert title here</title>
		<div class="modal-header">
			<h5 class="modal-title" id="ModalLabel"><%=pdt.getName() %></h5>
		</div>
		<form method="post" action="../../oin_makeOri">
			<table class="table table-striped">
				<tbody>
					<tr>
						<input type="hidden" name="pd_id"
							value=<%=x%>>
						<th scope="col" width="15%">
							<div class="form-row align-items-center">
								<label>容量</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt1"
											name="size" value="1" checked> <label> 700
											C.C. </label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt1"
											name="size" value="0"> <label> 500 C.C. </label>
									</div>
								</div>
							</div>
						</th>
					</tr>
				</tbody>

				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>溫度</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="6"> <label><a
											class="text-danger">熱飲</a></label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="5"> <label> 常溫</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="4"> <label> 去冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="3"> <label> 微冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="2"> <label> 少冰</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt2"
											name="tem" value="1" checked> <label> 正常</label>
									</div>
								</div>

							</div>
						</th>
					</tr>
				</tbody>

				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>甜度</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="5"> <label>多糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="1" checked> <label> 正常</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="2"> <label> 半糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="3"> <label> 微糖</label>
									</div>
								</div>
								<div class="col-auto my-1">
									<div class="form-check">
										<input class="form-check-radio" type="radio" id="opt3"
											name="sugar" value="4"> <label> 無糖</label>
									</div>
								</div>

							</div>
						</th>
					</tr>
				</tbody>

				<tbody>
					<tr>
						<th scope="col">
							<div class="form-row align-items-center">
								<label>加料</label>
							</div>
						</th>
						<th scope="col">
							<div class="form-row align-items-center">
								<div class="col-auto my-1">
									加料1:<select name="add1">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>
								</div>
								<div class="col-auto my-1">
									加料2:<select name="add2">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>
								</div>
								<div class="col-auto my-1">
									加料3:<select name="add3">
										<option value="1" selected>無
										<option value="2">椰果
										<option value="3">仙草
										<option value="4">芋圓
										<option value="5">布丁
										<option value="6">珍珠
									</select>

								</div>

							</div>
						</th>
					</tr>
				</tbody>
				<tbody>
					<tr>
						<th scope="col"><label>同款杯數</label></th>
						<th scope="col">
							<div class="form-row align-items-center">

								<div class="col-auto my-1">
								<div class="row">
								<select class="col custom-select  form-control-sm" id="qty10" name="qty10">
								<option value="0" selected="selected"></option>
								<option value="10" >1</option>
								<option value="20" >2</option>
								<option value="30" >3</option>
								<option value="40" >4</option>
								<option value="50" >5</option>
								<option value="60" >6</option>
								<option value="70" >7</option>
								<option value="80" >8</option>
								<option value="90" >9</option>
								</select>
								<select class="col custom-select  form-control-sm" id="qty1" name="qty1">
								<option value="0" >0</option>
								<option value="1" selected="selected">1</option>
								<option value="2" >2</option>
								<option value="3" >3</option>
								<option value="4" >4</option>
								<option value="5" >5</option>
								<option value="6" >6</option>
								<option value="7" >7</option>
								<option value="8" >8</option>
								<option value="9" >9</option>
								</select>
								</div>
								</div>
							</div>
						</th>
					</tr>
				</tbody>
			
				<tr>
					<th colspan=2>

						<hr class="mb-4">
						<div class="row justify-content-center">
							<div class="col-5">
								<button type="submit" class="btn btn-primary btn-lg btn-block" value="送出">送出
									</button></div>
								<div class="col-5">
									<button type=button class="btn btn-primary btn-lg btn-block"
										onclick="javascript:history.back(-1);" value="回上頁">回上頁</button>
								
							</div>
						
						</div>
						</th>
				</tr>

			</table>
		</form>
	</div>
</body>
<%@ include file="footer.jsp" %>
</html>