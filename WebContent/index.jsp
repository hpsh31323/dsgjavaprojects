<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%response.sendRedirect("dsg_001_inner/index.jsp"); %>

<html>
<meta http-equiv="refresh" content="0; url=./dsg_001/"> 
    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="./css/bootstrap.min.css" rel="stylesheet">
      <link href="./css/carousel.css" rel="stylesheet">
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
            <script src="https://code.jquery.com/jquery.js"></script>
            <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
            <script src="./js/bootstrap.min.js"></script>
   <head>
   
   <%@ include file="head.jsp"%>
  
 
   
   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">會員登入</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <form  action="login.html">
      <div class="modal-body">
                <div class="form-group">
                <label >電話</label>
                <input type="number" class="form-control" >
                </div>
                <div class="form-group">
                <label >密碼</label>
                <input type="password" class="form-control">
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary"  onclick="location.href='regist.html'">註冊</button>
        <button type="button" class="btn btn-primary">登入</button>
      </div>
      </form>
      
      
    </div>
  </div>
</div>
  </head>
   
   <body>
   
   
    <div class="container marketing">
    
    
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" >
    
    <div class="carousel-inner">
      <div class="carousel-item active" >
        <img class="d-block w-100" src="./img/3.jpg" alt="Third slide" height=100%>
        
        <div class="container">
          <div class="carousel-caption text-left">
            <p><a class="btn btn-lg btn-info alert" href="#" role="button">take break 放鬆心情</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
       <img class="d-block w-100" src="./img/2.jpg" alt="Third slide">
        <div class="container">
          <div class="carousel-caption">          
            <p><a class="btn btn-lg btn-warning alert" href="#" role="button">喝杯茶 再繼續</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
         <img class="d-block w-100" src="./img/1.jpg" alt="Third slide">
        <div class="container">
          <div class="carousel-caption text-right">         
            <p><a class="btn btn-lg btn-light alert" href="#" role="button">微糖 微冰，我是文青</a></p>
          </div>
        </div>
      </div>
    </div>
    
    
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
     </div>

 

    <!-- Three columns of text below the carousel -->
    <div class="row">
    
      <div class="col-lg-4">
         <img  src="./img/c.png" width="140" height="140" class="rounded-circle">
        <h2>外送服務</h2>
        <p>免打電話，服務到家，</p>
        <p><a class="btn btn-secondary" href="deliver.html" role="button">線上訂購</a></p>
      </div><!-- /.col-lg-4 -->
      
      <div class="col-lg-4"> 
       <img  src="./img/b.png" width="140" height="140" class="rounded-circle">
        <h2>預約訂購</h2>
        <p>線上訂購免排隊。會員小額支付</p>
        <p><a class="btn btn-secondary" href="reserve.html" role="button">自行取件</a></p>
      </div><!-- /.col-lg-4 -->
      
     <div class="col-lg-4"> 
         <img  src="./img/d.png" width="140" height="140"   class="rounded-circle">
        <h2>進度追蹤</h2>
        <p>外送急預約進度查詢</p>
        <p><a class="btn btn-secondary" href="tracking.html" role="button">立即查詢</a></p>
      </div><!-- /.col-lg-4 -->
    
    </div><!-- /.row -->


    <!-- START THE FEATURETTES -->

    <hr class="deliver-divider">
   <div class="row deliver"  style="box-shadow:inset 1px -1px 1px , inset -1px 1px 1px ; padding: 15px 15px 10px;">
      <div class="col-md-7">
        <h2 class="featurette-heading">新品推薦：<span class="text-muted">大小珍珠粉圓貢丸茶</span></h2>
        <p class="lead">所有口感一次滿足，話就當年白居易就是喝了這個，才想出“大珠小珠落玉盤”的千古絕句，為琵琶行畫龍點睛，傳頌至今。</p>
      </div>
      <div class="col-md-5">
      <img  src="./img/n.jpg" width="500" height="500" background="#eee" color="#aaa" class="bd-placeholder-img-lg featurette-image img-fluid mx-auto">
        
      </div>
    </div>

    <hr class="deliver-divider">
    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading">店長特調<span class="text-muted">小丸子愛撒嬌系列，內湖店限定</span></h2>
        <p class="lead">內湖店長本身營養學系出身，針對愛美女性推出保養茶飲，喝了就像小丸子，永遠年輕長不大，只在內湖店快來嘗鮮吧～</p>
      </div>
      <div class="col-md-5 order-md-1">
      <img  src="./img/p.jpg" width="500" height="500" background="#eee" color="#aaa" class="bd-placeholder-img-lg featurette-image img-fluid mx-auto">
      </div>
    </div>

    <hr class="deliver-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">台灣之光<span class="text-muted">美國門市接獲川普訂單</span></h2>
        <p class="lead">白宮消息指出，為慶祝歐巴馬卸任三週年，川普宴廣邀媒體餐敘，指名本店要黑黑黑奶茶，獻給歐巴馬和他妻子女兒，剛好三個黑！</p>
      </div>
      <div class="col-md-5">
     <img  src="./img/q.jpg" width="500" height="500" background="#eee" color="#aaa" class="bd-placeholder-img-lg featurette-image img-fluid mx-auto">
      </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->

  </div>

   <%@ include file="footer.jsp"%>
 
</main>
 
 
   </body>
</html>