<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>Insert title here</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="./css/bootstrap.min.css" rel="stylesheet">
      <link href="./css/carousel.css" rel="stylesheet">
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
            <script src="https://code.jquery.com/jquery.js"></script>
            <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
            <script src="./js/bootstrap.min.js"></script>
            
            
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#"><img src="./img/dsg_logo.png" height="50"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" href="#myModal">會員專區</a>     

        </li>
        
        <li class="nav-item active">
          <a class="nav-link" href="#">外送服務<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">自行取餐</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#">進度查詢<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">店家訊息</a>
        </li>
        
      </ul>
       <form class="form-inline mt-2 mt-md-0">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><img src="./img/cart.png" height="30"></button>
      </form>
    </div>     
 </nav>
</body>
</html>