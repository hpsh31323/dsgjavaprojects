package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Report_DB_Helper;

/**
 * Servlet implementation class report_singleitem
 */
@WebServlet("/report_singleitem")
public class report_singleitem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public report_singleitem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		int days = Integer.parseInt((request.getParameter("days")!=null)?request.getParameter("days"):"0");
		String year1 =(request.getParameter("yearfrom")!=null)?request.getParameter("yearfrom"):null;
		String month1 = (request.getParameter("monthfrom")!=null)?request.getParameter("monthfrom"):null;
		String day1 = (request.getParameter("dayfrom")!=null)?request.getParameter("dayfrom"):null; 
		String year2 = (request.getParameter("yearto")!=null)?request.getParameter("yearto"):null;
		String month2 = (request.getParameter("monthto")!=null)?request.getParameter("monthto"):null;
		String day2 = (request.getParameter("dayto")!=null)?request.getParameter("dayto"):null; 
		Report_By_Product [] rbps = null;
		if(session.getAttribute("employee")==null) {
			request.setAttribute("loginError", "閒置過久，網頁已過期");
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		try {
			Report_DB_Helper rdb = new Report_DB_Helper();
			if(days!=0) {
				rbps = rdb.queryProductByDays(days);
			}else {
				rbps = rdb.queryProductByWhen(year1, month1, day1, year2, month2, day2);
			}
			rdb.dsg_connect().close();
			request.setAttribute("year1", year1);
			request.setAttribute("month1", month1 );
			request.setAttribute("day1", day1 );
			request.setAttribute("year2", year2 );
			request.setAttribute("month2",month2 );
			request.setAttribute("day2",day2 );
			request.setAttribute("report_By_Products", rbps);
			request.setAttribute("pg", "2");
			request.getRequestDispatcher("report").forward(request, response);  
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("Error", "操作失敗，請重新操作");
			request.setAttribute("pg", "2");
			request.getRequestDispatcher("dsg_001_inner/boss/report.jsp").forward(request, response);
			e.printStackTrace();
		}
	}

}
