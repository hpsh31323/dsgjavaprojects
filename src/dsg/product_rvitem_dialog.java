package dsg;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Year;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import dsg_dbhelper.Product_DB_Helper;

/**
 * Servlet implementation class product_rvitem_dialog
 */
@WebServlet("/product_rvitem_dialog")
public class product_rvitem_dialog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public product_rvitem_dialog() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		FileInputStream fis =null;
		Employee [] employees= null;
		Product p = new Product();
		String pd_name = null;
		String note = null;
		String type = null;
		int price_l = 0;
		int price_m = 0;
		int class_id = 0;
		int cold = 0;
		int hot = 0;
		int p_id=0;
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		// 驗證請求是否滿足要求（post 請求 / enctype 是否以multipart打頭
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        // 如果不滿足要求就立即結束對該請求的處理
        if (!isMultipart) {
        	Error = "資料錯誤，請重新輸入";
			request.setAttribute("Error", Error);
			request.setAttribute("page", "2");
			request.getRequestDispatcher("product").forward(request, response);
        }
        try {
            // FileItem 是表單中的每一個元素的封裝
            // 創建一個 FileItem 的工廠類
            FileItemFactory factory = new DiskFileItemFactory();
            // 創建一個文檔上傳處理器（裝飾設計模式）
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setHeaderEncoding("UTF-8");
            
            // 解析請求
            List<FileItem> items = upload.parseRequest(request);
            for (FileItem fileItem : items) {
                // 判斷空間是否是普通控件
                if (fileItem.isFormField()) {
                    int x = Integer.parseInt(fileItem.getFieldName());
                    String value = fileItem.getString();
                    value = new String(value.getBytes("ISO-8859-1"),"UTF-8");
                    switch (x) {
					case 1:
						pd_name = value;
						break;

					case 2:
						class_id = Integer.parseInt(value);
						break;
					case 3:
						note = value;
						break;
					case 4:
						price_m = Integer.parseInt(value);
						break;
					case 5:
						price_l = Integer.parseInt(value);
						break;
					case 6:
						cold = (value!=null)?Integer.parseInt(value):0;
						break;
					case 7:
						hot = (value!=null)?Integer.parseInt(value):0;
						break;
					case 8:
						p_id = (value!=null)?Integer.parseInt(value):0;
						break;
					}
                } else {
                    String filename = fileItem.getName();
                	if(filename.endsWith(".jpg")) {
                		type = "jpg";
                	}else if(filename.endsWith(".png")){
                		type = "png";
                	}else if(filename.endsWith(".jpeg")){
                		type = "jpeg";
					}else {
						request.setAttribute("Error", "無效的圖檔，請上傳指定的圖檔類型");
						request.setAttribute("page", "2");
						request.getRequestDispatcher("product").forward(request, response);
					}
                	fis = (FileInputStream)fileItem.getInputStream();
                }
            }
           
            /////修改DB資料 ////
			Product_DB_Helper pdb = new Product_DB_Helper();
			String [] columns = {"pd_price_l","pd_price_m","pd_cold","pd_hot","pd_note","pd_class_id"};
			String [] values = {""+price_l,""+price_m,""+cold,""+hot,note,""+class_id};
			pdb.revise(columns, values, p_id);
			pdb.dsg_connect().close();
			/*
			if(fis!=null) {
				pdb = new Product_DB_Helper();
				pdb.revisePic(fis, p_id);
				pdb.dsg_connect().close();
			}*/
			request.setAttribute("message", "產品修改成功");
			request.setAttribute("page", "2");
			request.getRequestDispatcher("product").forward(request, response);      
        } catch (Exception e) {
        	Error = "新增失敗，請重新輸入";
			request.setAttribute("Error", Error);
			request.setAttribute("page", "2");
			request.getRequestDispatcher("product").forward(request, response);
            e.printStackTrace();
        }
	}

}
