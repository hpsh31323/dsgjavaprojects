package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;

/**
 * Servlet implementation class crew_admin
 */
@WebServlet("/crew_admin")
public class crew_admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crew_admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		String message = null;
		
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		Employee employee = (session.getAttribute("employee")!=null)?(Employee)session.getAttribute("employee"):null;
		String mail = employee.getMail();
		String path = "crew";
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		if(password.equals(employee.getPw())) {
			try {
				Employee_DB_Helper edb = new Employee_DB_Helper();
				String [] columns = {"E_pwd"};
				String [] values = {password1};
				edb.revise(columns, values, employee.getId());
				edb.dsg_connect().close();
				employee.setPw(password1);
				session.setAttribute("employee", employee);
				message = "密碼修改成功";
				request.setAttribute("message", message);
				request.setAttribute("pg", "3");
				request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else {
			Error = "密碼錯誤，請重新輸入";
			request.setAttribute("Error", Error);
			request.setAttribute("pg", "3");
			request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
		}
	}

}
