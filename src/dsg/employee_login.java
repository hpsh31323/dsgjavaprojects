package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;
import sun.util.resources.cldr.sw.CalendarData_sw_KE;

/**
 * Servlet implementation class employee_login
 */
@WebServlet("/employee_login")
public class employee_login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public employee_login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String loginError = null;
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		
		int person = (request.getParameter("person")!=null)?1:0;
		Employee employee =null;
		try {
			Employee_DB_Helper edb = new Employee_DB_Helper();
			employee = edb.queryEmployee(account);
			Employee [] employees = edb.queryAllIncumbentEmployee();
			edb.dsg_connect().close();
			if(employee.getPw().equals(password)) { // 此處如果使用者帳號輸入錯誤會產生error，需修正
				switch (person) {
				case 0:
					response.sendRedirect("dsg_001_inner/staff/index.jsp");
					break;
				case 1:
					switch(employee.getJob()) {
					case 1:
						session.setAttribute("employee", employee);
						session.setMaxInactiveInterval(5*60);
						response.sendRedirect("dsg_001_inner/staff/manage.jsp");
						break;
					case 2:
						session.setAttribute("employees", employees);
						session.setAttribute("employee", employee);
						session.setMaxInactiveInterval(5*60);
						response.sendRedirect("boss_index");
						//request.getRequestDispatcher("boss_index").forward(request, response);
						break;
					case 3:
						loginError = "非法帳號，人員已離職";
						request.setAttribute("loginError", loginError);
						request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
						break;
					}
					break;
				}
			}else {
				loginError = "帳號或密碼錯誤，請重新輸入";
				request.setAttribute("loginError", loginError);
				request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
			}
		} catch (ClassNotFoundException | SQLException e) {
			loginError = "帳號或密碼錯誤，請重新輸入";
			request.setAttribute("loginError", loginError);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
			e.printStackTrace();
		}
	}

}
