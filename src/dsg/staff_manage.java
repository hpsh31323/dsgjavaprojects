package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;

/**
 * Servlet implementation class staff_manage
 */
@WebServlet("/staff_manage")
public class staff_manage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public staff_manage() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/staff/manage.jsp").forward(request, response);
		}
		Employee employee = (session.getAttribute("employee")!=null)?(Employee)session.getAttribute("employee"):null;
		int e_id = employee.getId();
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String mail = request.getParameter("mail");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		if(password1.equals(employee.getPw())) {
			try {
				Employee_DB_Helper edb = new Employee_DB_Helper();
				String [] columns = {"E_Name","E_tel","E_mail","E_pwd"};
				String [] values = {name,phone,mail,(!password2.equals(""))?password2:password1};
				edb.revise(columns, values, employee.getId());
				employee = edb.queryEmployee(e_id);
				edb.dsg_connect().close();
				session.setAttribute("employee", employee);
				request.setAttribute("message", "個人資料修改成功");
				request.getRequestDispatcher("dsg_001_inner/staff/manage.jsp").forward(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else {
			Error = "原密碼錯誤，請重新輸入";
			request.setAttribute("Error", Error);
			request.getRequestDispatcher("dsg_001_inner/staff/manage.jsp").forward(request, response);
		}
		
	}

}
