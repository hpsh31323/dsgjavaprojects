package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Order_DB_Helper;

/**
 * Servlet implementation class ready_item
 */
@WebServlet("/ready_order")
public class ready_order extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ready_order() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		try {
			Order_DB_Helper db = new Order_DB_Helper();
			Order [] o_dev = db.queryReadyToDeliveryOrder(10);
			Order [] o_self = db.queryUntakenOrder(10);
			Order [] o_ondev = db.queryInDeliveryOrder(10);
			session.setAttribute("o_dev", o_dev);
			session.setAttribute("o_self", o_self);
			session.setAttribute("o_ondev", o_ondev);
			db.dsg_connect().close();
			//out.println(o_dev);
			//out.println(o_self);
			response.sendRedirect("dsg_001_inner/staff/waiting.jsp");
		} catch (ClassNotFoundException | SQLException e) {
			out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
