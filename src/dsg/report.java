package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Report_DB_Helper;

/**
 * Servlet implementation class report
 */
@WebServlet("/report")
public class report extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public report() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		Report_By_Order report_By_Order = null;
		Report_By_Product [] report_By_Products = null;
		String pg =  (request.getAttribute("pg")!=null)?(String)request.getAttribute("pg"):null;
		session.setAttribute("pg", pg);
		String year1 =(request.getAttribute("year1")!=null)?(String)request.getAttribute("year1"):null;
		String month1 = (request.getAttribute("month1")!=null)?(String)request.getAttribute("month1"):null;
		String day1 = (request.getAttribute("day1")!=null)?(String)request.getAttribute("day1"):null; 
		String year2 = (request.getAttribute("year2")!=null)?(String)request.getAttribute("year2"):null;
		String month2 = (request.getAttribute("month2")!=null)?(String)request.getAttribute("month2"):null;
		String day2 = (request.getAttribute("day2")!=null)?(String)request.getAttribute("day2"):null; 
		session.setAttribute("year1", year1);
		session.setAttribute("month1", month1);
		session.setAttribute("day1", day1);
		session.setAttribute("year2", year2);
		session.setAttribute("month2", month2);
		session.setAttribute("day2", day2);
		if(session.getAttribute("employee")==null) {
			request.setAttribute("loginError", "閒置過久，網頁已過期");
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		/////取出30天內訂單資料////
		try {
			Report_DB_Helper rdb = new Report_DB_Helper();
			report_By_Order = (request.getAttribute("report_By_Order")!=null)?(Report_By_Order)request.getAttribute("report_By_Order"):rdb.queryOrderByDays(30);
			report_By_Products = (request.getAttribute("report_By_Products")!=null)?(Report_By_Product[])request.getAttribute("report_By_Products"):rdb.queryProductByDays(30);
			rdb.dsg_connect().close();
			session.setAttribute("report_By_Order", report_By_Order);
			session.setAttribute("report_By_Products", report_By_Products);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		////////
		String message = (String)request.getAttribute("message");
		if(message==null) {
			response.sendRedirect("dsg_001_inner/boss/report.jsp");
			//request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
			//out.print(employee);
			}else {
				request.setAttribute("message", message);
				request.getRequestDispatcher("dsg_001_inner/boss/report.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
