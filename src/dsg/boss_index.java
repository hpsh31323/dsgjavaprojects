package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import dsg_dbhelper.DB_Helper;
import dsg_dbhelper.Report_DB_Helper;

/**
 * Servlet implementation class boss_index
 */
@WebServlet("/boss_index")
public class boss_index extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public boss_index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String loginError = null;
		Report_By_Order rbo_today = null;
		Report_By_Order rbo_yesterday = null;
		if(session.getAttribute("employee")==null) {
			loginError = "閒置過久，網頁已過期";
			request.setAttribute("loginError", loginError);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		try {
			Report_DB_Helper rdb = new Report_DB_Helper();
			rbo_today = rdb.queryOrderToday();
			rbo_yesterday = rdb.queryOrderYesterday();
			rdb.dsg_connect().close();
			session.setAttribute("rbo_today", rbo_today);
			session.setAttribute("rbo_yesterday", rbo_yesterday);
			response.sendRedirect("dsg_001_inner/boss/index.jsp");
			//request.getRequestDispatcher("dsg_001_inner/boss/index.jsp").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("loginError", "登入錯誤，請重新登入");
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
