package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.DB_Helper;
import dsg_dbhelper.Employee_DB_Helper;
import dsg_dbhelper.Product_DB_Helper;

/**
 * Servlet implementation class product
 */
@WebServlet("/product")
public class product extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public product() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		String pg =  (request.getAttribute("pg")!=null)?(String)request.getAttribute("pg"):null;
		session.setAttribute("pg", pg);
		String message = (request.getAttribute("message")!=null)?(String)request.getAttribute("message"):null;
		Employee [] employees= null;
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		
		try {
		/////取出所有產品類別資料////
			DB_Helper db = new DB_Helper();
			ResultSet rs = db.queryAll("product_class", null, null, null, null, null);
			ArrayList<String> as1 = new ArrayList<>();
			ArrayList<String> as2 = new ArrayList<>();
			while(rs.next()) {
				as1.add(rs.getString("class_name"));
				as2.add(rs.getString("class_note"));
			}
			rs.close();
			db.dsg_connect().close();
			String [] p_class = new String[as1.size()];
			String [] p_c_note = new String[as2.size()];
			for(int i = 0 ; i < as1.size() ; i++) {
				p_class[i]=as1.get(i);
				p_c_note[i]=as2.get(i);
			}
			/////////////////////////
			///////取出所有產品資料///////
			Product_DB_Helper pdb = new Product_DB_Helper();
			Product [] products = pdb.queryAllProducts();
			pdb.dsg_connect().close();
			///////////////////////////
			session.setAttribute("products", products);
			session.setAttribute("p_class", p_class);
			session.setAttribute("p_c_note", p_c_note);
			Error = (request.getAttribute("Error")!=null)?(String)request.getAttribute("Error"):null;
			//response.sendRedirect("dsg_001_inner/boss/product.jsp");
			if(message==null && Error==null) {
				response.sendRedirect("dsg_001_inner/boss/product.jsp");
			}else {
				if(message!=null)request.setAttribute("message", message);
				if(Error !=null)request.setAttribute("Error", Error);
				request.getRequestDispatcher("dsg_001_inner/boss/product.jsp").forward(request, response); 
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
