package dsg;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;
import dsg_dbhelper.Product_DB_Helper;
////目前停用///
/**
 * Servlet implementation class product_rvitem_delete
 */
@WebServlet("/index_getback_pwd")
public class index_getback_pwd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public index_getback_pwd() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		String account = request.getParameter("account");
		String mail = request.getParameter("mail");
		String path = "dsg_001_inner/index.jsp";
		/////////產生8位數亂數密碼///////
		String e_pw="";
		for(int i=0;i<8;)
		{
			int x= (int)(Math.random()*(122-48+1)+48);
			if(x<58 || x>64 & x<91 || x>97 ) {
				char a = (char) x;
				e_pw += a;
				i++;
			}
		}
		/////////確認帳號資料///////////
		try {
			Employee_DB_Helper edb = new Employee_DB_Helper(); 
			Employee ee =edb.queryEmployee(account);
			if(ee.getMail().equals(mail)) {
				edb.simpleRevise("employee", "E_pwd", e_pw, "Employee_ID", ee.getId());
				///////////寄出密碼信//////////
		        String to = mail;
		        String subject = "取回密碼";
		        String text = ee.getName()+" 您好:\n"+
		        			 "     以下為您的新密碼\n\n"+
		        			 "     員工密碼:"+e_pw+"\n\n"+
		    	        			 "請儘速至 http://localhost:8080/DSGJavaProjects/dsg_001_inner/ 修改密碼";
				String msg = "密碼已發送至信箱";
		        request.setAttribute("path", path);
				request.setAttribute("to", to);
				request.setAttribute("subject", subject);
				request.setAttribute("text", text);
				request.setAttribute("msg", msg);
				request.getRequestDispatcher("mail").forward(request, response);
			}else {
				request.setAttribute("Error", "帳號或信箱位址錯誤，請重新輸入");
				request.getRequestDispatcher("dsg_001_inner/logrtv.jsp").forward(request, response);
			}
			
		} catch (Exception e) {
			request.setAttribute("Error", "帳號或信箱位址錯誤，請重新輸入");
			request.getRequestDispatcher("dsg_001_inner/logrtv.jsp").forward(request, response);
			e.printStackTrace();
		}
		
		
	}

}
