package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Order_DB_Helper;

/**
 * Servlet implementation class deliverlist
 */
@WebServlet("/deliverlist")
public class deliverlist extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deliverlist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		String message = (request.getAttribute("message")!=null)?(String)request.getAttribute("message"):null;
		Employee [] employees= null;
		Order [] o_ondev = null;
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		try {
			Order_DB_Helper odb =new Order_DB_Helper();
			o_ondev = odb.queryInDeliveryOrder(20);
			odb.dsg_connect().close();
			session.setAttribute("o_ondev", o_ondev);
			Error = (request.getAttribute("Error")!=null)?(String)request.getAttribute("Error"):null;
			//response.sendRedirect("dsg_001_inner/boss/product.jsp");
			if(message==null && Error==null) {
				response.sendRedirect("dsg_001_inner/staff/deliverlist.jsp");
			}else {
				if(message!=null)request.setAttribute("message", message);
				if(Error !=null)request.setAttribute("Error", Error);
				request.getRequestDispatcher("dsg_001_inner/staff/deliverlist.jsp").forward(request, response); 
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
