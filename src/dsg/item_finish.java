package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Order_DB_Helper;
import dsg_dbhelper.Order_Info_DB_Helper;

/**
 * Servlet implementation class item_finish
 */
@WebServlet("/item_finish")
public class item_finish extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public item_finish() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session = request.getSession();
		PrintWriter out = response.getWriter();
		try {
			Order_Info_DB_Helper db = new Order_Info_DB_Helper();
			int i_id = Integer.parseInt(request.getParameter("item"));
			int o_id = db.queryOrderInfoByInfoID(i_id).getO_id();
			db.setFinish(i_id);
			db.dsg_connect().close();
			Order_DB_Helper dbo =new Order_DB_Helper();
			dbo.isFinish(o_id);
			dbo.dsg_connect().close();
			response.sendRedirect("waiting_list");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
