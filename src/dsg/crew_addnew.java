package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;

/**
 * Servlet implementation class crew_addnew
 */
@WebServlet("/crew_addnew")
public class crew_addnew extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crew_addnew() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		Employee employee = (session.getAttribute("employee")!=null)?(Employee)session.getAttribute("employee"):null;
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String mail = request.getParameter("mail");
		String path = "crew";
		/////////產生8位數亂數密碼///////
		String e_pw="";
		for(int i=0;i<8;)
		{
			int x= (int)(Math.random()*(122-48+1)+48);
			if(x<58 || x>64 & x<91 || x>97 ) {
				char a = (char) x;
				e_pw += a;
				i++;
			}
		}
		/////////寫入DB///////////
		Employee ee = new Employee(name, e_pw, phone, mail, 1);
		try {
			Employee_DB_Helper edb = new Employee_DB_Helper();
			edb.add(ee);
			ee.setAccount(edb.queryEmployee(edb.getID("employee", "Employee_ID")).getAccount());
			edb.dsg_connect().close();
	///////////寄出密碼信//////////
	        String to = ee.getMail();
	        String subject = "員工帳號＆員工密碼";
	        String text = ee.getName()+" 您好:\n"+
	        			 "     以下為您的員工帳號＆員工密碼\n\n"+
	        			 "     員工帳號:"+ee.getAccount()+"\n"+
	        			 "     員工密碼:"+ee.getPw()+"\n\n"+
	    	        			 "請儘速至 http://localhost:8080/DSGJavaProjects/dsg_001_inner/ 修改密碼";
			String msg = "員工新增成功";
	        request.setAttribute("path", path);
			request.setAttribute("to", to);
			request.setAttribute("subject", subject);
			request.setAttribute("text", text);
			request.setAttribute("msg", msg);
			request.getRequestDispatcher("mail").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			Error = "新增失敗，請重新操作";
			request.setAttribute("Error", Error);
			request.setAttribute("pg", "1");
			request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
		}
	}

}
