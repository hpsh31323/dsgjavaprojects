package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.DB_Helper;

/**
 * Servlet implementation class product_catset_revise
 */
@WebServlet("/product_catset_revise")
public class product_catset_revise extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public product_catset_revise() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		int class_id = Integer.parseInt(request.getParameter("class_id"));
		String class_name = (request.getParameter("class_name")!=null)?request.getParameter("class_name"):"";
		String class_note = (request.getParameter("note")!=null)?request.getParameter("note"):"";
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		try {
			DB_Helper db =new DB_Helper();
			db.simpleRevise("product_class", "class_name", class_name, "pd_class_id", class_id);
			db.simpleRevise("product_class", "class_note", class_note, "pd_class_id", class_id);
			db.dsg_connect().close();
			request.setAttribute("message", "類別說明修改成功");
			request.setAttribute("pg", "3");
			request.getRequestDispatcher("product").forward(request, response);
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("Error", "修改失敗，請重新輸入");
			request.setAttribute("pg", "3");
			request.getRequestDispatcher("product").forward(request, response);
			e.printStackTrace();
		}
	}

}
