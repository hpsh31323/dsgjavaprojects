package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Product_DB_Helper;

/**
 * Servlet implementation class product_rvitem
 */
@WebServlet("/product_rvitem")
public class product_rvitem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public product_rvitem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		int pd_id  = Integer.parseInt(request.getParameter("pd_id"));
		int price_l = Integer.parseInt(request.getParameter("price_l"));
		int price_m = Integer.parseInt(request.getParameter("price_m"));
		int class_id = Integer.parseInt(request.getParameter("pd_class"));
		int pd_avaliable = Integer.parseInt(request.getParameter("pd_avaliable"));
		int cold = (request.getParameter("pd_cold")!=null)?Integer.parseInt(request.getParameter("pd_cold")):0;
		int hot = (request.getParameter("pd_hot")!=null)?Integer.parseInt(request.getParameter("pd_hot")):0;
		int pd_priority =(Integer.parseInt(request.getParameter("pd_priority")));
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		try {
			Product_DB_Helper pdb = new Product_DB_Helper();
			String [] columns = {"pd_price_l","pd_price_m","pd_priority","pd_avaliable","pd_cold","pd_hot","pd_class_id"};
			String [] values = {""+price_l,""+price_m,""+pd_priority,""+pd_avaliable,""+cold,""+hot,""+class_id};
			pdb.revise(columns, values, pd_id);
			pdb.dsg_connect().close();
			request.setAttribute("message", "產品修改成功");
			request.setAttribute("pg", "2");
			request.getRequestDispatcher("product").forward(request, response);  
		} catch (ClassNotFoundException | SQLException e) {
			request.setAttribute("Error", "修改失敗，請重新輸入");
			request.setAttribute("pg", "2");
			request.getRequestDispatcher("dsg_001_inner/boss/product.jsp").forward(request, response);
			e.printStackTrace();
		}
		

	}

}
