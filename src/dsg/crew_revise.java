package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;

/**
 * Servlet implementation class crew_revise
 */
@WebServlet("/crew_revise")
public class crew_revise extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crew_revise() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String Error = null;
		
		if(session.getAttribute("employee")==null) {
			Error = "閒置過久，網頁已過期";
			request.setAttribute("loginError", Error);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		Employee employee = (session.getAttribute("employee")!=null)?(Employee)session.getAttribute("employee"):null;
		int e_id = Integer.parseInt(request.getParameter("e_id"));
		int incumbent = Integer.parseInt(request.getParameter("incumbent")); //在職:1 ; 離職:2
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String mail = request.getParameter("mail");
		String [] columns = {"E_Name","E_tel","E_mail","Job_ID"};
		String [] values = {name,phone,mail,(incumbent==1)?"1":"3"};
		try {
			Employee_DB_Helper edb = new Employee_DB_Helper();
			edb.revise(columns, values, e_id);
			Employee [] employees= edb.queryAllIncumbentEmployee();
			session.setAttribute("employees", employees);
			edb.dsg_connect().close();
			request.setAttribute("message", "修改成功");
			request.setAttribute("pg", "2");
            request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);	
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
