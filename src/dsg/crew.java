package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Employee_DB_Helper;

/**
 * Servlet implementation class crew
 */
@WebServlet("/crew")
public class crew extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crew() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		String pg =  (request.getAttribute("pg")!=null)?(String)request.getAttribute("pg"):null;
		session.setAttribute("pg", pg);
		String loginError = null;
		Employee [] employees= null;
		if(session.getAttribute("employee")==null) {
			loginError = "閒置過久，網頁已過期";
			request.setAttribute("loginError", loginError);
			request.getRequestDispatcher("dsg_001_inner/index.jsp").forward(request, response);
		}
		Employee employee = (session.getAttribute("employee")!=null)?(Employee)session.getAttribute("employee"):null;
		//session.setAttribute("employee", employee);
		/////取出所有在職員工資料////
		try {
			Employee_DB_Helper edb = new Employee_DB_Helper();
			employees = edb.queryAllIncumbentEmployee();
			edb.dsg_connect().close();
			session.setAttribute("employees", employees);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		////////
		String message = (String)request.getAttribute("message");
		if(message==null) {
			response.sendRedirect("dsg_001_inner/boss/crew.jsp");
			//request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
			//out.print(employee);
			}else {
				request.setAttribute("message", message);
				request.getRequestDispatcher("dsg_001_inner/boss/crew.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
