package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Order_DB_Helper;

/**
 * Servlet implementation class bad_debts
 */
@WebServlet("/bad_debts")
public class bad_debts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public bad_debts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		int o_id = Integer.parseInt(request.getParameter("item"));
		String note = (request.getParameter("note")!=null)?request.getParameter("note"):"";
		try {
			Order_DB_Helper odb =new Order_DB_Helper();
			odb.simpleRevise("dsg_order", "Order_States_ID", "8", "Order_ID",o_id);
			odb.simpleRevise("dsg_order", "O_note", note, "Order_ID", o_id);
			Order [] o_bad = odb.queryDevFailedOrder(50);
			odb.dsg_connect().close();
			session.setAttribute("o_bad", o_bad);
			response.sendRedirect("dsg_001_inner/staff/unfinish.jsp");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
