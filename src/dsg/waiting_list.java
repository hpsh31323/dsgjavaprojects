package dsg;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.Order_Info_DB_Helper;

/**
 * Servlet implementation class waiting_list
 */
@WebServlet("/waiting_list")
public class waiting_list extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public waiting_list() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();
		try {
			Order_Info_DB_Helper db =new Order_Info_DB_Helper();
			OrderInfo [] infos = db.queryInProductionOrder(10);
			session.setAttribute("infos", infos);
			db.dsg_connect().close();
			//out.print(infos);
			response.sendRedirect("dsg_001_inner/staff/operation.jsp");
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			out.println("error");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
