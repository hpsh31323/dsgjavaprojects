package oin_srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Order;
import dsg.OrderInfo;

/**
 * Servlet implementation class oin_editOri
 */
@WebServlet("/oin_editOri")
public class oin_editOri extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public oin_editOri() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		
		//ori.setPd_id(Integer.parseInt(request.getParameter("id")));待對應之PD_ID設定完成後解除
		int pd_id=Integer.parseInt(request.getParameter("pd_id"));
		int s_id=Integer.parseInt(request.getParameter("sugar"));
		int t_id=Integer.parseInt(request.getParameter("tem"));
		int qty=Integer.parseInt(request.getParameter("qty"));
	//抓取陣列使用request.getParameterValues
	
		int add1=Integer.parseInt(request.getParameter("add1"));
		int add2=Integer.parseInt(request.getParameter("add2"));
		int add3=Integer.parseInt(request.getParameter("add3"));
		int size=Integer.parseInt(request.getParameter("size"));
		
System.out.println("check1");
		try 
			{
					Order ord=(Order)request.getSession().getAttribute("ord");
					//OrderInfo ori = new OrderInfo(s_id,t_id,pd_id,add1,add2,add3,size,qty);//建立物件
					@SuppressWarnings("unchecked")
					ArrayList<OrderInfo> ora=(ArrayList<OrderInfo>)request.getSession().getAttribute("ora");
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setS_id(s_id);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setT_id(t_id);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setAdd1_id(add1);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setAdd2_id(add2);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setAdd3_id(add3);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setSize(size);
					ora.get(Integer.parseInt(request.getParameter("pd_id"))).setQty(qty);
					System.out.println("check2");
					
					//呼叫Order物件，並存放至ArrayList中。
					//Order ord=(Order)request.getSession().getAttribute("ord");
					
					
					
					//int sum=ord.getSum()+ora.get(Integer.parseInt(request.getParameter("pd_id"))).getItem_price();//記錄總金額
					//ord.setSum(sum);
					//System.out.println(ord.getSum());
					
					//System.out.println(ora.isEmpty());
					session.setAttribute("ord", ord);
					session.setAttribute("ora", ora);
					System.out.println("check3");
					response.sendRedirect("dsg_001_inner/staff/order_checklist.jsp");
		
			}
		catch (NumberFormatException | ClassNotFoundException | SQLException e) 
		{
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
}
