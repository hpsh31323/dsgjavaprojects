package oin_srv;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg_dbhelper.DB_Helper;

/**
 * Servlet implementation class order
 */
@WebServlet("/order")
public class order extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public order() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		ResultSet rs;
		ArrayList<String> pd_name=new ArrayList<>();	
		ArrayList<String> pd_id_list=new ArrayList<>();
		HttpSession session = request.getSession();
		try {
			DB_Helper dbh = new DB_Helper();
			if (Integer.parseInt(request.getParameter("class_id")) != 0) {
				rs = dbh.queryAll("product_menu", "pd_class_id", "=", request.getParameter("class_id"), null, null);
			} else {
				rs = dbh.queryAll("product_menu", null, null, null, null, null);
			}
			
			while(rs.next())
			{
				pd_name.add(rs.getString("pd_name"));
				pd_id_list.add(rs.getString("Product_id"));
			}
			session.setAttribute("pd_name", pd_name);
			session.setAttribute("pd_id_list", pd_id_list);
			rs.close();//關閉resultSet
			dbh.dsg_connect().close();//關閉connection物件
		} catch (SQLException |ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		response.sendRedirect("dsg_001_inner/staff/order.jsp");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

	}

}
