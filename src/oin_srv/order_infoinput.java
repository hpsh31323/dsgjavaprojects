package oin_srv;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Product;
import dsg_dbhelper.Product_DB_Helper;

/**
 * Servlet implementation class order_infoinput
 */
@WebServlet("/order_infoinput")
public class order_infoinput extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public order_infoinput() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int pd_id=Integer.parseInt(request.getParameter("pd_idd"));
		Product_DB_Helper pdh;
		HttpSession session = request.getSession();
		try {
			pdh = new Product_DB_Helper();
			Product pdt=pdh.queryProduct(pd_id);
			
			session.setAttribute("pd_id", pd_id);
			session.setAttribute("pdt", pdt);
			pdh.dsg_connect().close();
			System.out.println(pd_id);
			response.sendRedirect("dsg_001_inner/staff/order_infoinput.jsp");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
