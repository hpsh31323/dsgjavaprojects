package oin_srv;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Order;
import dsg.OrderInfo;
import dsg_dbhelper.DB_Helper;

/**
 * Servlet implementation class order_front
 */
@WebServlet("/order_front")
public class order_front extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public order_front() {
		super();
		// TODO Auto-generated constructor stub
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Order ord = new Order();
		int customer_id = 1; // 店家點單預設為1
		ArrayList<OrderInfo> ora = new ArrayList<OrderInfo>();
		HttpSession session=request.getSession();
		ord.setCustomer(customer_id);
		request.getSession().setAttribute("ora", ora);
		request.getSession().setAttribute("ord", ord);
		ResultSet  rs_class;
		
		ArrayList<String> pd_class=new ArrayList<>();
		
		try {
			DB_Helper dbh = new DB_Helper();
			rs_class = dbh.queryAll("product_class", null, null, null, null, null);
			
			while(rs_class.next()) {
				pd_class.add(rs_class.getString("class_name"));
			}
			session.setAttribute("pd_class",pd_class);
			rs_class.close();//關閉resultSet
			dbh.dsg_connect().close();//關閉connection物件
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.sendRedirect("order?class_id=1");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

	}

}
