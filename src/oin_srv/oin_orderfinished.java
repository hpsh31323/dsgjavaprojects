package oin_srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Order;
import dsg.OrderInfo;
import dsg_dbhelper.Order_DB_Helper;

/**
 * Servlet implementation class oin_orderfinished
 */
@WebServlet("/oin_orderfinished")
public class oin_orderfinished extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public oin_orderfinished() {
        super();
        // TODO Auto-generated constructor stub
    }
 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	HttpSession session=request.getSession();
    	 Order ord = (Order)session.getAttribute("ord");
    	 ArrayList<OrderInfo> ora = (ArrayList<OrderInfo>)session.getAttribute("ora");
    	 ord.setTaxid((String)request.getParameter("taxid"));
    	 ord.setNote((String)request.getParameter("note"));
    	 System.out.println(ord.getTaxid());
    	 System.out.println(ord.getNote());
    	 ord.setItem(ora);
    	 System.out.println(ord.getAdd());
    	 try {
			Order_DB_Helper odh=new Order_DB_Helper();
			if(odh.add(ord)==1)
				{
					System.out.println("完成");
					odh.dsg_connect().close();
					response.sendRedirect("dsg_001_inner/staff/order_front.jsp");
				}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
