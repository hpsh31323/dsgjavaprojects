package oin_srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Order;
import dsg.OrderInfo;

/**
 * Servlet implementation class oin_makeOri
 */
@WebServlet("/oin_makeOri")
public class oin_makeOri extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public oin_makeOri() {
        super();
        // TODO Auto-generated constructor stub
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		
		//ori.setPd_id(Integer.parseInt(request.getParameter("id")));待對應之PD_ID設定完成後解除
		int pd_id=Integer.parseInt(request.getParameter("pd_id"));
		int s_id=Integer.parseInt(request.getParameter("sugar"));
		int t_id=Integer.parseInt(request.getParameter("tem"));
		int qty=Integer.parseInt(request.getParameter("qty1"))+Integer.parseInt(request.getParameter("qty10"));
	//抓取陣列使用request.getParameterValues
	
		int add1=Integer.parseInt(request.getParameter("add1"));
		int add2=Integer.parseInt(request.getParameter("add2"));
		int add3=Integer.parseInt(request.getParameter("add3"));
		int size=Integer.parseInt(request.getParameter("size"));
		
		System.out.println(size);
		try 
			{
					Order ord=(Order)request.getSession().getAttribute("ord");
					OrderInfo ori = new OrderInfo(s_id,t_id,pd_id,add1,add2,add3,size,qty);//建立物件
					int item_total;
					item_total=ori.getItem_price();
					System.out.println(pd_id);
					//呼叫Order物件，並存放至ArrayList中。
					//Order ord=(Order)request.getSession().getAttribute("ord");
					@SuppressWarnings("unchecked")
					ArrayList<OrderInfo> ora=(ArrayList<OrderInfo>)request.getSession().getAttribute("ora");
					System.out.println(ori.getItem_price());
					
					//int sum=ord.getSum()+ori.getItem_price();//記錄總金額
					//ord.setSum(sum);
					//System.out.println(ord.getSum());
					
					//System.out.println(ora.isEmpty());
					ora.add(ori);
					session.setAttribute("ord", ord);
					session.setAttribute("ora", ora);
					response.sendRedirect("dsg_001_inner/staff/order.jsp?class_id=0");
			} 
		catch (NumberFormatException | ClassNotFoundException | SQLException e) 
			{
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
}
