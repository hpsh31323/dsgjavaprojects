package oin_srv;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dsg.Order;
import dsg.OrderInfo;

/**
 * Servlet implementation class oin_delete
 */
@WebServlet("/oin_delete")
public class oin_delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public oin_delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Order ord = (Order)session.getAttribute("ord");
		ArrayList<OrderInfo> ora=(ArrayList<OrderInfo>)session.getAttribute("ora");
		int temp_sum=ord.getSum();
		temp_sum-=ora.get(Integer.parseInt(request.getParameter("id"))).getItem_price();
		ord.setSum(temp_sum);
		ora.remove(Integer.parseInt(request.getParameter("id")));
		session.setAttribute("ora", ora);
		session.setAttribute("ord", ord);
		response.sendRedirect("dsg_001_inner/staff/order_checklist.jsp");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
